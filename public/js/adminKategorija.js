function obrisiSliku(nazivSlike){
    $.ajax({
        url: "/admin/kategorija/obrisiUploadSlike",
        type: "POST",
        data: {
            "filename": nazivSlike,
        }
    });

    // nemoj sa JQuery posto u ID-ju postoji tacka
    document.getElementById('slike-' + nazivSlike).remove();
}

function promeniTekstUStablu(){
    changingNode.text = $('#naziv').val();

    // poziva se samo da bi se azurirao tekst
    $('#treeview-selectable').treeview('disableNode', changingNode);
}

function formaKategorijaPoslata(){
    let selectedNodes = $selectableTree.treeview('getSelected');

    $forma = $('#forma-kategorija');

    $forma.append('<input type="hidden" name="kategorija" value="' +  selectedNodes[0].tags +'" />\n');

    return true;
}