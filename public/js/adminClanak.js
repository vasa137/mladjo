function obrisiSliku(nazivSlike){
    $.ajax({
        url: "/admin/clanak/obrisiUploadSlike",
        type: "POST",
        data: {
            "filename": nazivSlike,
        }
    });

    // nemoj sa JQuery posto u ID-ju postoji tacka
    document.getElementById('slike-' + nazivSlike).remove();
}