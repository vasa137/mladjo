<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StavkaOpcija extends Model
{
    protected $table = 'stavka_opcija';

    protected $fillable = ['id_stavka_porudzbina', 'id_opcija'];

    public static function dohvatiOpcijeZaStavku($id){
        return StavkaOpcija::where('id_stavka_porudzbina', $id)->get();
    }
}
