<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 03-Apr-19
 * Time: 15:06
 */

namespace App;


class Izvestaj
{
    public $objekat;
    public $porudzbina;
    public $komada;
    public $razlika;
    public $ukupno;

    /**
     * Izvestaj constructor.
     * @param $objekat
     * @param $porudzbina
     * @param $komada
     * @param $razlika
     * @param $ukupno
     */
    public function __construct($objekat, $porudzbina, $komada, $razlika, $ukupno)
    {
        $this->objekat = $objekat;
        $this->porudzbina = $porudzbina;
        $this->komada = $komada;
        $this->razlika = $razlika;
        $this->ukupno = $ukupno;
    }


    /**
     * @return mixed
     */
    public function getObjekat()
    {
        return $this->objekat;
    }

    /**
     * @param mixed $objekat
     */
    public function setObjekat($objekat): void
    {
        $this->objekat = $objekat;
    }



    /**
     * @return mixed
     */
    public function getPorudzbina()
    {
        return $this->porudzbina;
    }

    /**
     * @param mixed $porudzbina
     */
    public function setPorudzbina($porudzbina): void
    {
        $this->porudzbina = $porudzbina;
    }

    /**
     * @return mixed
     */
    public function getKomada()
    {
        return $this->komada;
    }

    /**
     * @param mixed $komada
     */
    public function setKomada($komada): void
    {
        $this->komada = $komada;
    }

    /**
     * @return mixed
     */
    public function getRazlika()
    {
        return $this->razlika;
    }

    /**
     * @param mixed $razlika
     */
    public function setRazlika($razlika): void
    {
        $this->razlika = $razlika;
    }

    /**
     * @return mixed
     */
    public function getUkupno()
    {
        return $this->ukupno;
    }

    /**
     * @param mixed $ukupno
     */
    public function setUkupno($ukupno): void
    {
        $this->ukupno = $ukupno;
    }



}