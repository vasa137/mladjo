<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpisProizvoda extends Model
{
    protected $table = 'opis_proizvoda';

    protected $fillable = ['opis', 'napomena', 'boja_pozadine', 'boja_slova'];

    public static function dohvatiSve(){
        return OpisProizvoda::all();
    }

    public static function dohvatiSaId($id){
        return OpisProizvoda::where('id', $id)->first();
    }

}
