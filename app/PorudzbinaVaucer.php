<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class PorudzbinaVaucer extends Model
{
    protected $table = 'porudzbina_vaucer';

    protected $fillable = ['id_porudzbina', 'id_vaucer'];

    public static function dohvatiVaucereZaPorudzbinu($id){
        return PorudzbinaVaucer::where('id_porudzbina', $id)->get();
    }

    public static function dohvatiIzvestajZaVaucere($datumOd, $datumDo){
        return DB::select("
            select v.id as id_vaucer, IFNULL(SUM(v.iznos),0) as ukupno_ustedjeno, IFNULL(COUNT(distinct p.id), 0) as porudzbina
            from porudzbina_vaucer pv, vaucer v, porudzbina p
            where pv.id_vaucer = v.id
            and pv.id_porudzbina = p.id
            and p.status <> 'stornirana'
            and DATE(p.created_at) >= '$datumOd'
            and DATE(p.created_at) < '$datumDo' 
            group by v.id
        ");
    }
}
