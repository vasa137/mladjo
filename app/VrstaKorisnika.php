<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VrstaKorisnika extends Model
{
    protected $table = 'vrsta_korisnika';

    protected $fillable = ['naziv', 'opis'];

    public static function dohvatiSaId($id){
        return VrstaKorisnika::where('id', $id)->first();
    }

    public static function dohvatiSve(){
        return VrstaKorisnika::all();
    }
}
