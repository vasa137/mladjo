<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaucer extends Model
{
    protected $table = 'vaucer';

    protected $fillable = ['naziv', 'kod', 'aktivan', 'sakriven', 'iznos', 'ukupno_ustedjeno'];

    protected $ukupno_ustedjeno;

    public function setUkupnoUstedjenoAttribute($ukupno_ustedjeno){
        $this->ukupno_ustedjeno = $ukupno_ustedjeno;
    }

    public function getUkupnoUstedjenoAttribute(){
        return $this->ukupno_ustedjeno;
    }

    public static function dohvatiSaId($id){
        return Vaucer::where('id', $id)->first();
    }

    public static function dohvatiSaKodom($kod){
        return Vaucer::where('kod', $kod)->first();
    }

    public static function daLiJeJedinstven($kod){
        return self::dohvatiSaKodom($kod) != null ? false : true;
    }

    public function napuni($naziv, $kod, $iznos){
        $this->naziv = $naziv;
        $this->kod = $kod;
        $this->iznos = $iznos;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }

    public function iskoristi(){
        $this->aktivan = 0;

        $this->save();
    }

    public function daLiJeValidan($kod){
        $vaucer = self::dohvatiSaKodom($kod);

        return $vaucer->aktivan == 1 && $vaucer->sakriven == 0;
    }

    public static  function dohvatiSveNeobrisane(){
        return Vaucer::where('sakriven', 0)->get();
    }

    public static  function dohvatiSveObrisane(){
        return Vaucer::where('sakriven', 1)->get();
    }
}
