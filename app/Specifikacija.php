<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specifikacija extends Model
{
    protected $table = 'specifikacija';

    protected $fillable = ['naziv', 'opis', 'sakriven'];

    public static function dohvatiSveAktivne(){
        return Specifikacija::where('sakriven', 0)->get();
    }

    public static function dohvatiSpecifikaciju($id){
        return Specifikacija::where('id', $id)->first();
    }

    public static function dohvatiSveObrisane(){
        return Specifikacija::where('sakriven', 1)->get();
    }

    public static function dohvatiSaId($id){
        return Specifikacija::where('id', $id)->first();
    }


    public function napuni($naziv, $opis){
        $this->naziv = $naziv;
        $this->opis = $opis;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
