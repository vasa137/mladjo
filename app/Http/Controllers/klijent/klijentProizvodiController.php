<?php

namespace App\Http\Controllers\klijent;

use App\Brend;
use App\Kategorija;
use App\Proizvod;
use App\Specifikacija;
use App\ProizvodKategorija;
use App\ProizvodSpecifikacija;
use App\Utility\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
define("PAGINACIJA", 12);
class klijentProizvodiController extends Controller
{
    private function popuniProizvodeNizomKategorija($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvodKategorije = ProizvodKategorija::dohvatiKategorijeZaProizvod($proizvod->id);
            $nizKategorijaProizvod = [];

            foreach($proizvodKategorije as $proizvodKategorija){
                $putanjaKategorijaNiz = Util::getInstance()->buildCategoryIdParentArray($proizvodKategorija->id_kategorija);
                $nizKategorijaProizvod = array_merge($nizKategorijaProizvod, $putanjaKategorijaNiz);
            }

            $nizKategorijaProizvod = array_unique($nizKategorijaProizvod);

            $proizvod->kategorije = $nizKategorijaProizvod;
        }
    }

    private function popuniProizvodeBrendovima($proizvodi){
        foreach($proizvodi as $proizvod){
            if($proizvod->id_brend != null) {
                $proizvod->brend = Brend::dohvatiSaId($proizvod->id_brend);
            }
        }
    }

    private function popuniProizvodeLinkovima($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvod->link = "PUZZLE SHOP - ";
            if($proizvod->kategorije != null) {
                
                foreach($proizvod->kategorije as $k)
                {
                    $proizvod->link=$proizvod->link.Kategorija::dohvatiSaId($k)->naziv."-";
                }
                $proizvod->link=$proizvod->link.$proizvod->naziv." ".$proizvod->sifra;
            }
        }
    }

    private function popuniProizvodeNazivimaGlavnihSlika($proizvodi){
        foreach($proizvodi as $proizvod){
            $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);
        }
    }

    private function odrediBrojProizvodaZaKategorijeUzFiltriranje($kategorijeZaFilter, $brendoviZaFilter, $kategorije, $glavne){
        $mapa = [];

        foreach($kategorije as $kategorija){
            $mapa[$kategorija->id] = $kategorija;
            $kategorija->broj_proizvoda = 0;
        }

        foreach($glavne as $idGlavnaKat){

            if(isset($kategorijeZaFilter[$idGlavnaKat])) {
                $kopija = $kategorijeZaFilter[$idGlavnaKat];

                $kategorijeZaFilter[$idGlavnaKat] = [];

                $rezultat = Proizvod::filtrirajBrojKategorija($kategorijeZaFilter, $brendoviZaFilter, $idGlavnaKat);

                $kategorijeZaFilter[$idGlavnaKat] = $kopija;
            } else{
                // REZULTAT BEZ IZBACIVANJA, ZA GLAVNE KATEGORIJE KOJE NEMAJU NISTA PODSELEKTOVANO ZA SAD
                $rezultat = Proizvod::filtrirajBrojKategorija($kategorijeZaFilter, $brendoviZaFilter, $idGlavnaKat);
            }

            foreach ($rezultat as $rez) {
                $kategorija = $mapa[$rez->idKategorija];

                // AKO JE NADKATEGORIJA
                if($kategorija->id == $idGlavnaKat){
                    $kategorija->broj_proizvoda += $rez->broj_proizvoda;
                }
                // AKO JE PODKATEGORIJA, MORA SE POVECATI I BROJ ZA NADKATEGORIJU ZATO JE GORE ISTO +=
                else{
                    $kategorija->broj_proizvoda = $rez->broj_proizvoda;
                    $mapa[$kategorija->id_nad_kategorija]->broj_proizvoda += $rez->broj_proizvoda;
                }

            }

        }
    }

    private function odrediBrojProizvodaZaBrendove($kategorijeZaFilter, $brendovi){
        $ukupno = 0;

        $mapa = [];

        foreach($brendovi as $brend){
            $mapa[$brend->id] = $brend;
            $brend->broj_proizvoda = 0;
        }

        $rezultat = Proizvod::filtrirajBrojBrendova($kategorijeZaFilter);

        foreach($rezultat as $rez){
            $brend = $mapa[$rez->idBrend];

            $brend->broj_proizvoda = $rez->broj_proizvoda;

            $ukupno += $brend->broj_proizvoda;
        }

        return $ukupno;
    }

    private function daLiTrebaCheckBoxZaGlavneOdvojene($kategorije, $glavneOdvojene){
        $checkboxZaGlavneOdvojene = false;

        $brojGlavnihSaBarJednimProizvodom = 0;

        foreach($kategorije as $kategorija){
            if(in_array($kategorija->id, $glavneOdvojene) && $kategorija->broj_proizvoda > 0){
                $brojGlavnihSaBarJednimProizvodom++;

                if($brojGlavnihSaBarJednimProizvodom > 1){
                    $checkboxZaGlavneOdvojene = true;
                    break;
                }
            }
        }

        return $checkboxZaGlavneOdvojene;
    }

    public function prodavnica(Request $request){
        $kategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();

        $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

        $brendovi = Brend::dohvatiSveAktivne();

        // ZA OVE KATEGORIJE NA POCETKU PRIKAZATI SAMO CHECKBOX ZA NADKATEGORIJU
        $glavneOdvojene = [Kategorija::$PUZLE_ZA_DECU, Kategorija::$PUZLE_ZA_ODRASLE, Kategorija::$PRIBOR];

        if ($request->ajax()) {
            $queryKategorije = $request->query('kategorije');
            $queryBrendovi = $request->query('brendovi');
            $querySort = $request->query('sort');
            $queryGlavne = $request->query('glavne');

            if($querySort == "null"){
                $querySort = null;
            }

            $kategorijeZaFilter = [];

            if($queryGlavne != null) {
                $izabraneKategorije = [];
                $viseOtvorenihGlavnihOdvojenih = false;
                $brojOtvorenihGlavnihOdvojenih = 0;
                foreach($queryGlavne as $queryGlavna){
                    if(in_array($queryGlavna, $glavneOdvojene)){
                        $brojOtvorenihGlavnihOdvojenih++;

                        if($brojOtvorenihGlavnihOdvojenih > 1){
                            $viseOtvorenihGlavnihOdvojenih = true;
                            break;
                        }
                    }
                }

                if($viseOtvorenihGlavnihOdvojenih){
                    $izabranaNekaOdGlavnih = false;
                    foreach($glavneOdvojene as $glavnaOdvojena){
                        if(isset($queryKategorije[$glavnaOdvojena])){
                            // imace samo jedan izabrani element jer su vise glavnih otvorene pa ne moze u tom koraku dva da budu izabrana
                            $izabranaUGlavnojOdvojenoj = $queryKategorije[$glavnaOdvojena][0];

                            $izabranaNekaOdGlavnih = true;
                            $nizKategorijaFilter = [];
                            Util::getInstance()->buildCategoryIdSubtreeArray($kategorije, intval($izabranaUGlavnojOdvojenoj), $nizKategorijaFilter);
                            $kategorijeZaFilter[$glavnaOdvojena] = $nizKategorijaFilter;
                            $izabraneKategorije = array_merge($izabraneKategorije, [$izabranaUGlavnojOdvojenoj]);
                            break;
                        }
                    }
                }

                if(!$viseOtvorenihGlavnihOdvojenih || !$izabranaNekaOdGlavnih) {
                    foreach($queryGlavne as $idKat) {
                        if (in_array($idKat, $glavneOdvojene) && !isset($queryKategorije[$idKat])) {
                            $nizKategorijaFilter = [];
                            Util::getInstance()->buildCategoryIdSubtreeArray($kategorije, intval($idKat), $nizKategorijaFilter);
                            $kategorijeZaFilter[$idKat] = $nizKategorijaFilter;
                        }
                    }
                }


                // PODRAZUMEVA SE DA CE SVI PROIZVODI BITI SMESTENI U GLAVNE ODVOJENE KATEGORIJE

            }



            if($queryKategorije != null){
                foreach($queryKategorije as $key => $nizKategorija){
                    if(!isset($kategorijeZaFilter[$key])) {
                        $kategorijeZaFilter[$key] = $nizKategorija;
                        $izabraneKategorije = array_merge($izabraneKategorije, $nizKategorija);
                    }
                }
            }

            //dd($kategorijeZaFilter);
            $brendoviZaFilter = [];
            $izabraniBrendovi = [];

            if($queryBrendovi != null){
                $brendoviZaFilter = $queryBrendovi;
                $izabraniBrendovi = $queryBrendovi;
            }

            $sortBy = "created_at";
            $redosled = "desc";

            if($querySort != null){
                $explodedSort = explode('-', $querySort);

                $sortBy = $explodedSort[0];
                $redosled = $explodedSort[1];
            }

            $proizvodi = Proizvod::filtriraj($kategorijeZaFilter, $brendoviZaFilter, $sortBy, $redosled, PAGINACIJA);

            $this->odrediBrojProizvodaZaKategorijeUzFiltriranje($kategorijeZaFilter, $brendoviZaFilter, $kategorije, $queryGlavne);

            $ukupnoProizvodaBrendovi = $this->odrediBrojProizvodaZaBrendove($kategorijeZaFilter, $brendovi);

            $checkboxZaGlavneOdvojene = $this->daLiTrebaCheckBoxZaGlavneOdvojene($kategorije, $glavneOdvojene);

            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);
            $this->popuniProizvodeBrendovima($proizvodi);
            $this->popuniProizvodeNizomKategorija($proizvodi);
            $this->popuniProizvodeLinkovima($proizvodi);

            $proizvodiView = view('include.listaProizvoda',compact('proizvodi'))->render();
            $filteriView = view('include.filterTabla', compact('stabloKategorija', 'brendovi', 'ukupnoProizvodaBrendovi', 'izabraneKategorije', 'izabraniBrendovi', 'checkboxZaGlavneOdvojene', 'glavneOdvojene'))->render();

            return response()->json(['proizvodiHtml' => $proizvodiView, 'filteriHtml' => $filteriView ]);
        } else {
            $queryKategorija = $request->query('kategorija');
            $queryBrend = $request->query('brend');

            if($queryKategorija != null){
                $brendoviZaFilter = $izabraniBrendovi = [];
                $izabraneKategorije = [$queryKategorija];
                $kategorija = Kategorija::dohvatiSaId($queryKategorija);

                $nizKategorijaFilter = [];
                Util::getInstance()->buildCategoryIdSubtreeArray($kategorije, intval($queryKategorija), $nizKategorijaFilter);

                $kategorijeZaFilter = [];

                if($kategorija->id_nad_kategorija == null) {
                    $kategorijeZaFilter[$kategorija->id] = $nizKategorijaFilter;
                } else{
                    $kategorijeZaFilter[$kategorija->id_nad_kategorija] = $nizKategorijaFilter;
                }

                $proizvodi = Proizvod::filtriraj($kategorijeZaFilter, [], 'created_at', 'desc', PAGINACIJA);
            } else if($queryBrend != null){
                $brendoviZaFilter = $izabraniBrendovi = [$queryBrend];
                $kategorijeZaFilter = $izabraneKategorije = [];

                /*
                foreach($stabloKategorija as $glavnaKat){
                    if(in_array($glavnaKat->id, $glavneOdvojene)){
                        $glavnaKat->children = [];
                    }
                }
                */

                $proizvodi = Proizvod::filtriraj([], [$queryBrend], 'created_at', 'desc',PAGINACIJA);
            }

            if( ($queryKategorija == null && $queryBrend == null) || (count($proizvodi) == 0)){
                $brendoviZaFilter = $kategorijeZaFilter = $izabraneKategorije = $izabraniBrendovi = [];
                $proizvodi = Proizvod::filtriraj([], [], 'created_at', 'desc',PAGINACIJA);
            }

            // ZA ODREDJIVANJE BROJ PROIZVODA POSALJI SVE GLAVNE NIJE VAZNO NEK SE RACUNA DUZE
            $glavne = Kategorija::dohvatiSveKategorijePoPrioritetuNaNivou(0);

            $glavneNiz = [];

            foreach($glavne as $glavna){
                $glavneNiz[] = $glavna->id;
            }

            $this->odrediBrojProizvodaZaKategorijeUzFiltriranje($kategorijeZaFilter, $brendoviZaFilter, $kategorije, $glavneNiz);

            $ukupnoProizvodaBrendovi = $this->odrediBrojProizvodaZaBrendove($kategorijeZaFilter, $brendovi);

            $checkboxZaGlavneOdvojene = $this->daLiTrebaCheckBoxZaGlavneOdvojene($kategorije, $glavneOdvojene);

            $this->popuniProizvodeNazivimaGlavnihSlika($proizvodi);
            $this->popuniProizvodeBrendovima($proizvodi);
            $this->popuniProizvodeNizomKategorija($proizvodi);
            $this->popuniProizvodeLinkovima($proizvodi);


            return view('prodavnica', compact('stabloKategorija', 'brendovi', 'proizvodi', 'izabraneKategorije', 'izabraniBrendovi', 'ukupnoProizvodaBrendovi' , 'checkboxZaGlavneOdvojene', 'glavneOdvojene'));
        }
    }

    public function proizvod($link, $id){
        $proizvod = Proizvod::dohvatiSaId($id);

        if($proizvod == null || $proizvod->sakriven){
            abort(404);
        }

        $proizvodKategorije = ProizvodKategorija::dohvatiKategorijeZaProizvod($id);

        $kategorije = [];

        foreach($proizvodKategorije as $proizvodKategorija){
            $kategorije [] = Kategorija::dohvatiSaId($proizvodKategorija->id_kategorija);
        }

        $proizvod->kategorije = $kategorije;

        if($proizvod->id_brend != null){
            $proizvod->brend = Brend::dohvatiSaId($proizvod->id_brend);
        }

        if ($proizvod->ima_specifikacije) {
            $proizvodNizSpecifikacija = [];
            $proizvodNizSpecifikacijaTekst = [];
            $proizvodSpecifikacije = ProizvodSpecifikacija::dohvatiSpecifikacijeZaProizvod($proizvod->id);

            foreach ($proizvodSpecifikacije as $proizvodSpecifikacija) {
                $proizvodNizSpecifikacija[] = Specifikacija::dohvatiSpecifikaciju($proizvodSpecifikacija->id_specifikacija)->naziv;
                $proizvodNizSpecifikacijaTekst[] = $proizvodSpecifikacija->tekst;
                
            }

            $proizvod->specifikacije = $proizvodNizSpecifikacija;
            $proizvod->specifikacije_tekst = $proizvodNizSpecifikacijaTekst;
        }

        $proizvodDirectory =  public_path('images/proizvodi/' . $proizvod->id);

        $proizvod->nazivGlavneSlike = Util::getInstance()->nazivGlavneSlike($proizvod);

        $proizvod->sveSlike = Util::getInstance()->pokupiNizFajlova($proizvodDirectory . '/sveSlike');

        //dd($proizvod);

        return view('proizvod', compact('proizvod'));
    }
}
