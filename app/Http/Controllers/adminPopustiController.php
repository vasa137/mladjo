<?php

namespace App\Http\Controllers;

use App\KuponKategorija;
use App\KuponKorisnik;
use App\KuponProizvod;
use App\Proizvod;
use App\User;
use Illuminate\Http\Request;
use App\Kategorija;
use App\Utility\Util;
use App\Kupon;
use App\Vaucer;
use Redirect;
class adminPopustiController extends Controller
{
    //


    public function kupon($id)
    {
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $kategorije = Kategorija::dohvatiSveAktivne();
        $stabloKategorija = Util::getInstance()->buildCategoryTree($kategorije, null);

        $proizvodi = Proizvod::dohvatiSveAktivne();
        $korisnici = User::dohvatiSveKojiNisuAdmini();

        if(!$izmena){
            return view('admin.adminKupon', compact('izmena', 'stabloKategorija', 'proizvodi', 'korisnici'));
        } else{
            $kupon = Kupon::dohvatiSaId($id);

            if($kupon == null){
                abort(404);
            }

            if(!$kupon->za_sve_kategorije){
                $nizKategorija = [];
                $kuponKategorije = KuponKategorija::dohvatiKategorijeZaKupon($id);

                foreach($kuponKategorije as $kuponKategorija){
                    $nizKategorija[] = $kuponKategorija->id_kategorija;
                }

                $kupon->kategorije = $nizKategorija;
            }

            if(!$kupon->za_sve_kupce){
                $nizKupaca = [];
                $kuponKorisnici = KuponKorisnik::dohvatiKorisnikeZaKupon($id);

                foreach($kuponKorisnici as $kuponKorisnik){
                    $nizKupaca [] = $kuponKorisnik->id_user;
                }

                $kupon->korisnici = $nizKupaca;
            }

            if($kupon->jos_proizvoda){
                $nizProizvoda = [];
                $kuponProizvodi = KuponProizvod::dohvatiProizvodeZaKupon($id);

                foreach($kuponProizvodi as $kuponProizvod){
                    $nizProizvoda[] = $kuponProizvod->id_proizvod;
                }

                $kupon->proizvodi = $nizProizvoda;
            }


            return view('admin.adminKupon', compact('izmena', 'kupon', 'stabloKategorija', 'proizvodi', 'korisnici'));
        }

    }

    public function sacuvaj_kupon($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $kod = $_POST['kod'];
        $popust = $_POST['popust'];

        $popust = floatval($popust) / 100;

        $ogranicen = false;
        $max_koriscenja = null;

        $broj_koriscenja = $_POST['broj_koriscenja'];

        if(isset($_POST['ogranicen'])){
            $ogranicen = true;
            $max_koriscenja = $_POST['max_koriscenja'];
        }

        $za_sve_kategorije = $_POST['za_sve_kategorije'];

        $jos_proizvoda = false;


        if($za_sve_kategorije == 0) {
            $kategorije = [];
            if (isset($_POST['kategorije'])) {
                $kategorije = $_POST['kategorije'];
            }

            if(isset($_POST['jos_proizvoda'])){
                $jos_proizvoda = true;
                $proizvodi = $_POST['proizvodi'];
            }
        }

        $za_snizene = $_POST['za_snizene'];
        $za_sve_kupce= $_POST['za_sve_kupce'];

        if($za_sve_kupce == 0){
            $korisnici = $_POST['korisnici'];
        }

        $jedinstven = true;

        if($izmena){
            $kupon = Kupon::dohvatiSaId($id);

            if($kod != $kupon->kod){
                $jedinstven = Kupon::daLiJeJedinstven($kod);
            }

            if($ogranicen && $max_koriscenja < $broj_koriscenja){
                return Redirect::back()->withErrors(['greska' => 'Broj korišćenja kupona ne može biti veći od maksimalnog broja korišćenja.']);
            }
        } else {
            $kupon = new Kupon();

            $jedinstven = Kupon::daLiJeJedinstven($kod);
        }

        if(!$jedinstven){
            // obavezno znak => stavi u withErrors
            return Redirect::back()->withErrors(['greska' => 'Kod kupona mora biti jedinstven.']);
        }

        $kupon->napuni($naziv, $kod, $popust, $ogranicen, $broj_koriscenja, $max_koriscenja, $za_sve_kategorije, $jos_proizvoda, $za_sve_kupce, $za_snizene);

        if($izmena){
            KuponKategorija::obrisiKategorijeZaKupon($kupon->id);
        }

        if($za_sve_kategorije == 0){
            foreach($kategorije as $idKategorije){
                $kuponKategorija = new KuponKategorija();
                $kuponKategorija->napuni($kupon->id, $idKategorije);
            }
        }

        if($izmena){
            KuponKorisnik::obrisiKorisnikeZaKupon($kupon->id);
        }

        if($za_sve_kupce == 0){
            foreach($korisnici as $idKorisnik){
                $kuponKorisnik = new KuponKorisnik();
                $kuponKorisnik->napuni($kupon->id, $idKorisnik);
            }
        }

        if($izmena){
            KuponProizvod::obrisiProizvodeZaKupon($kupon->id);
        }

        if($jos_proizvoda){
            foreach($proizvodi as $idProizvod){
                $kuponProizvod = new KuponProizvod();
                $kuponProizvod->napuni($kupon->id, $idProizvod);
            }
        }

        return redirect('/admin/kupon/' . $kupon->id);
    }

    public function obrisi_kupon($id)
    {
        $kupon = Kupon::dohvatiSaId($id);

        $kupon->obrisi();

        return Redirect::back();
    }

    public function restauriraj_kupon($id)
    {
        $kupon = Kupon::dohvatiSaId($id);

        $kupon->restauriraj();

        return Redirect::back();
    }

    public function aktiviraj_kupon($id)
    {
        $kupon = Kupon::dohvatiSaId($id);

        $kupon->aktiviraj();

        return Redirect::back();
    }

    public function deaktiviraj_kupon($id)
    {
        $kupon = Kupon::dohvatiSaId($id);

        $kupon->deaktiviraj();

        return Redirect::back();
    }


    public function kuponi()
    {
    	$kuponi = Kupon::dohvatiSveNeobrisane();
    	$obrisaniKuponi = Kupon::dohvatiSveObrisane();

    	$brojAktivnih = 0;

    	foreach($kuponi as $kupon){
    	    if($kupon->aktivan){
    	        $brojAktivnih++;
            }
        }

    	return view('admin.adminKuponi', compact('kuponi', 'obrisaniKuponi', 'brojAktivnih'));
    }


    public function vaucer($id)
    {
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        if(!$izmena){
            return view('admin.adminVaucer', compact('izmena'));
        } else{
            $vaucer = Vaucer::dohvatiSaId($id);

            if($vaucer == null){
                abort(404);
            }

            return view('admin.adminVaucer', compact('izmena', 'vaucer'));
        }
    }

    public function sacuvaj_vaucer($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $kod = $_POST['kod'];
        $iznos = $_POST['iznos'];

        $jedinstven = true;

        if(!$izmena) {
            $vaucer = new Vaucer();

            $jedinstven = Vaucer::daLiJeJedinstven($kod);
        } else{
            $vaucer = Vaucer::dohvatiSaId($id);

            if($vaucer->kod != $kod){
                $jedinstven = Vaucer::daLiJeJedinstven($kod);
            }
        }

        if(!$jedinstven){
            // obavezno znak => stavi u withErrors
            return Redirect::back()->withErrors(['greska' => 'Kod vaučera mora biti jedinstven.']);
        }

        $vaucer->napuni($naziv, $kod, $iznos);

        return redirect('/admin/vaucer/' . $vaucer->id);
    }

    public function obrisi_vaucer($id)
    {
        $vaucer = Vaucer::dohvatiSaId($id);

        $vaucer->obrisi();

        return Redirect::back();
    }

    public function restauriraj_vaucer($id)
    {
        $vaucer = Vaucer::dohvatiSaId($id);

        $vaucer->restauriraj();

        return Redirect::back();
    }

    public function iskoristi_vaucer($id)
    {
        $vaucer = Vaucer::dohvatiSaId($id);

        $vaucer->iskoristi();

        return Redirect::back();
    }

    public function vauceri()
    {
    	$vauceri = Vaucer::dohvatiSveNeobrisane();

    	$obrisaniVauceri = Vaucer::dohvatiSveObrisane();

    	$brojAktivnih = 0;

    	foreach($vauceri as $vaucer){
    	    if($vaucer->aktivan){
    	        $brojAktivnih++;
            }
        }

    	return view('admin.adminVauceri', compact('vauceri', 'obrisaniVauceri', 'brojAktivnih'));
    }


}
