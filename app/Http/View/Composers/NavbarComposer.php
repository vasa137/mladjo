<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 18-Apr-19
 * Time: 23:07
 */

namespace App\Http\View\Composers;

use App\Brend;
use App\Kategorija;
use App\Utility\Util;
use Illuminate\View\View;


class NavbarComposer
{


    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $navbarKategorije = Kategorija::dohvatiAktivneKategorijeSortiranePoPrioritetu();

        $navbarKategorije = Util::getInstance()->buildCategoryTree($navbarKategorije, null);

        $navbarBrendovi = Brend::dohvatiSveAktivne();

        $glavneKojeSeMoguIzabrati = Kategorija::$GLAVNE_KOJE_SE_MOGU_IZABRATI;

        $view->with('navbarKategorije', $navbarKategorije);
        $view->with('navbarBrendovi', $navbarBrendovi);
        $view->with('glavneKojeSeMoguIzabrati', $glavneKojeSeMoguIzabrati);
    }
}