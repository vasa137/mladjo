@extends('layout')

@section('body')
class="index-opt-1 catalog-product-view catalog-view_op1"
@stop

@section('title')
Proizvod  - 
@stop

@section('main')

<div class="columns container">
    <!-- Block  Breadcrumb-->

    <ol class="breadcrumb no-hide">
        <li><a href="/">Naslovna</a></li>
        @if(!empty($proizvod->kategorije))
            
                @foreach($proizvod->kategorije as $kategorija)
                <li>
                    <a href="/prodavnica?kategorija={{$kategorija->id}}">
                    {{$kategorija->naziv}}
                                                        
                    @if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
                    @endif
                    </a>
                </li>
                @endforeach
            
        @endif
        <li class="active">{{$proizvod->naziv}}</li>
    </ol>


    <div class="row">

        

        <!-- Main Content -->
        <div class="col-md-12  col-main">

            <div class="row">
        
                <div class="col-sm-6 col-md-4 col-lg-4">

                    <div class="product-media media-horizontal">

                        <div class="image_preview_container images-large">
                            @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
                                <img id="img_zoom" data-zoom-image="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}">
                            @endif
                            <button class="btn-zoom open_qv"><span>zoom</span></button>

                        </div>
                        
                        <div class="product_preview images-small">

                            <div class="owl-carousel thumbnails_carousel" id="thumbnails"  data-nav="true" data-dots="false" data-margin="10" data-responsive='{"0":{"items":3},"480":{"items":4},"600":{"items":5},"768":{"items":3}}'>
                                @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
                                    <a href="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" data-image="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')}}" data-zoom-image="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}">

                                        <img src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" data-large-image="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}">

                                    </a>
                                @endif

                                @foreach($proizvod->sveSlike as $slika)
                                <a href="/images/proizvodi/{{$proizvod->id}}/sveSlike/{{$slika}}" data-image="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" data-zoom-image="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}">

                                    <img src="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" data-large-image="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" alt="">

                                </a>
                                @endforeach

                            </div><!--/ .owl-carousel-->

                        </div><!--/ .product_preview-->

                    </div><!-- image product -->
                </div>

                <div class="col-sm-6 col-md-5 col-lg-5"> 

                    <div class="product-info-main">

                        <h1 class="page-title">
                           @if($proizvod->id_brend != null) {{$proizvod->brend->naziv}} Puzzle @endif {{$proizvod->naziv}} {{$proizvod->sifra}}
                        </h1>
                        
                       
                        <div class="product-info-price">
                            <div class="price-box">
                                
                                <br>
                                @if($proizvod->na_popustu)
                                <span class="price">{{number_format($proizvod->cena_popust,0,',', '.')}} rsd</span>
                                <span class="old-price">{{number_format($proizvod->cena,0,',','.')}} rsd</span>
                                <span class="label-sale">-{{number_format((($proizvod->cena - $proizvod->cena_popust)/ $proizvod->cena * 100) ,0,',', '.')}}% (ušteda {{number_format($proizvod->cena - $proizvod->cena_popust,0, ',', '.')}} rsd)</span>
                                @else
                                    <span class="price">{{number_format($proizvod->cena,0, ',', '.')}} rsd</span>
                                @endif
                                
                            </div>
                        </div>
                        <a type="submit">
                                     <img width="160" src="{{asset('/images/kupi1.png')}}">
                                </a>
                        

                        <div class="product-code">
                            Šifra artikla: {{$proizvod->sifra}}
                        </div>
                        <div class="product-condition">
                            Brend: {{$proizvod->brend->naziv}}
                        </div>
                        @if($proizvod->ima_specifikacije)
                            <?php for($i = 0; $i < count($proizvod->specifikacije); $i++) {?>
                                <div class="product-condition">
                                   {{$proizvod->specifikacije[$i]}}: {{$proizvod->specifikacije_tekst[$i]}}
                                </div>
                            <?php } ?>
                        @endif

                        @if(!empty($proizvod->kategorije))
                            <div class="product-condition">
                                Kategorije:
                                @foreach($proizvod->kategorije as $kategorija)
                                    {{$kategorija->naziv}}
                                    @if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
                                        ,
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        <img src="{{asset('/images/b1.png')}}"> 
                        <div class="product-overview">
                            <div class="overview-content">
                                {{$proizvod->opis}}
                            </div>
                        </div>
                        
                        <div class="">
                            <form>
                                <!-- mislim da nece da ima kolicinu za dodavanje u korpu
                                <div class="product-options-wrapper">

                                    
                                    <div class="form-qty">
                                        <label class="label">Količina: </label>
                                        <div class="control">
                                            <input type="text" class="form-control input-qty" value='1' id="qty1" name="qty1"  maxlength="12"  minlength="1">
                                            <button class="btn-number  qtyminus" data-type="minus" data-field="qty1"><span>-</span></button>
                                            <button class="btn-number  qtyplus" data-type="plus" data-field="qty1"><span>+</span></button>
                                        </div>
                                    </div>
                                    
                                </div>
                                -->
                                

                                <div class="product-options-bottom clearfix">
                                    
                                    <div class="actions">
                                        
                                       
                                        
                                    </div>
                                   
                                </div>
                                <img src="{{asset('/images/b2.png')}}">
                            </form>
                        </div>
                        <!--
                        <div class="share">
                            <img src="images/media/index1/share.png" alt="share">
                        </div>
                        -->

                    </div><!-- detail- product -->

                </div><!-- Main detail -->
                <div style="border:2px solid black; text-align: center;" class="col-sm-6 col-md-3 col-lg-3">
                <br> 
                    <img style="display: block;
                              margin-left: auto;
                              margin-right: auto;
                              " height="140" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" data-large-image="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}">
                        
                        
                        <h1 >+</h1>
                        
                        <img style="display: block;
                              margin-left: auto;
                              margin-right: auto;
                              width: 50%;" height="140" src="{{asset('/images/podloga.jpg')}}">
                        <br>
                        <span style="background-color: #CA363E; padding: 10px 30px 10px 30px; margin: 5px ; color: white;" class="label-sale"><strong>-5% ušteda 300 rsd</strong></span>
                        <h1>= 4500 rsd</h1><br>
                        <a type="submit">
                                     <img width="100%" src="{{asset('/images/kupi1.png')}}">
                                </a>
                </div>

            </div>
<br>
            <div class="container ">
                <div class="block-service-opt1">
                    <div class="clearfix">
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <span class="icon">
                                    <img src="{{asset('images/media/index1/besplatna-dostava.png')}}" alt="service">
                                </span>
                                <strong class="title">besplatna dostava </strong>
                                <span>Za iznose preko 3000 rsd</span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <span class="icon">
                                    <img src="{{asset('images/media/index1/service2.png')}}" alt="service">
                                </span>
                                <strong class="title">Vraćamo novac!</strong>
                                <span>Moneyback guarantee</span>
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <span class="icon">
                                    <img src="{{asset('images/media/index1/service4.png')}}" alt="service">
                                </span>
                                <strong class="title">SIGURNA KUPOVINA </strong>
                                <span>SSL Sertifikovana prodavnica</span>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- block  service-->
            


            <div style="border:2px solid black; text-align: center; " class="block-banner-opt1 effect-banner3 row">
                
                <div class="col-md-2 col-sm-6">
                <img 
                          style="display: block;
                          margin-left: auto;
                          margin-right: auto;
                          height: 180px;
                          padding-bottom: 30px;
                          padding-top: 30px;
                          width: 70%;" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" data-large-image="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}">
                </div>

                <div class="col-md-1 col-sm-6">
                <h1 style="padding-top: 70%;">+</h1>
                </div>

                <div class="col-md-2 col-sm-6">
                <img style="display: block;
                          margin-left: auto;
                          margin-right: auto;
                          padding-bottom: 30px;
                          padding-top: 30px;
                          height: 180px;
                          width: 70%;"
                            src="{{asset('/images/podloga.jpg')}}">
                </div>

                 <div class="col-md-1 col-sm-6">
                <h1 style="padding-top: 70%;">+</h1>
                </div>

                <div class="col-md-2 col-sm-6">
                <img style="display: block;
                          margin-left: auto;
                          margin-right: auto;
                          padding-bottom: 30px;
                          padding-top: 30px;
                          height: 180px;
                          width: 70%;"
                            src="{{asset('/images/lepak.jpg')}}">
                </div>

                <div  class="col-md-3 col-sm-6">
                    <h1 style="padding-top: 18%">= 5900 rsd</h1>
                <span style="background-color: #CA363E; padding: 10px 30px 10px 30px; margin: 40px ; color: white;" class="label-sale"><strong>-5% ušteda 550 rsd</strong></span>
                
                </div>
            </div>


            <br>

            
            
            
            <div class="block-upsell ">
                <div class="block-title">
                    <strong class="title">Pogledali ste ranije:</strong>
                </div>
                <div class="block-content ">
                    <ol class="product-items owl-carousel " data-nav="true" data-dots="false" data-margin="30" data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":3},"992":{"items":3},"1200":{"items":4}}'>
                        
                        
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                        
                    
                    </ol>
                </div>
            </div><!-- block-Upsell Products -->


            <!-- Banner -->
            <div class="block-banner-opt1 effect-banner3">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="#" class="box-img"><img src="{{asset('images/akcija1.jpg')}}" alt="banner"></a>
                        </div>
                        <div class="col-sm-6">
                            <a href="#" class="box-img"><img src="{{asset('images/akcija2.jpg')}}" alt="banner"></a>
                        </div>
                    </div>
                </div>
            </div><!-- Banner -->

            <div class="block-upsell ">
                <div class="block-title">
                    <strong class="title">Slični proizvodi:</strong>
                </div>
                <div class="block-content ">
                    <ol class="product-items owl-carousel " data-nav="true" data-dots="false" data-margin="30" data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":3},"992":{"items":3},"1200":{"items":4}}'>
                        
                        
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>

                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="product-item product-item-opt-2">
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                        
                    
                    </ol>
                </div>
            </div><!-- block-Upsell Products -->

        </div><!-- Main Content -->
        
       

    </div>
</div>




        

@stop