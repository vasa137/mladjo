@extends('layout')



@section('title')
Korpa
@stop

@section('main')
<div class="columns container">
    <!-- Block  Breadcrumb-->
            
    <ol class="breadcrumb no-hide">
        <li><a href="/">Naslovna    </a></li>
        <li class="active"> Korpa</li>
    </ol><!-- Block  Breadcrumb-->

    <div class="page-content checkout-page">
    	
        <div class="col-sm-8">
		    <h3 class="checkout-sep">KORPA</h3>
		    <div class="box-border">
		        <div class="table-responsive">
		            <table class="table table-bordered  cart_summary">
		                <thead>
		                    <tr>
		                        <th class="cart_product">Proizvod</th>
		                        <th>Naziv</th>
		                        
		                        <th>Cena</th>
		                        <th>Količina</th>
		                        <th>Ukupno</th>
		                        <th class="action"><i class="fa fa-trash-o"></i></th>
		                    </tr>
		                </thead>
		                <tbody>
		                    <tr>
		                        <td class="cart_product">
		                            <a href="#"><img src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg" height="70" width="100%"></a>
		                        </td>
		                        <td class="cart_description">
		                            <p class="product-name"><a href="#">Frederique Constant </a></p>
		                            
		                        </td>
		                       
		                        <td class="price"><span>1000</span></td>
		                        <td class="qty">
		                           
		                            <input minlength="1" maxlength="12" name="qty1" id="qty1" value="1" class="form-control input-sm" type="text">
		                            <span data-field="qty1" data-type="minus" class="btn-number" ><i class="fa fa-caret-up"></i></span>
		                            <span data-field="qty1" data-type="plus" class="btn-number"><i class="fa fa-caret-down"></i></span>
		                        </td>
		                        <td class="price">
		                            <span>2000</span>
		                        </td>
		                        <td class="action">
		                            <a href="#">Ukloni</a>
		                        </td>
		                    </tr>
		                    
		                </tbody>
		                <tfoot>

		                    <tr>
		                        
		                        <td colspan="4">Cena</td>
		                        <td colspan="3">4600</td>
		                    </tr>
		                    <tr>
		                        <td colspan="4">Dostava</td>
		                        <td colspan="3">300</td>
		                    </tr>
		                    <tr>
		                    	
		                        <td colspan="4"><strong>UKUPNO</strong></td>
		                        <td colspan="3"><strong>3900</strong></td>
		                    </tr>
		                </tfoot>    
		            </table>
		            
		        </div>
		    </div>
		</div>
		<div class="col-sm-4">
	        <h3 class="checkout-sep">Informacije za dostavu</h3>
	        <div class="box-border">
	            <ul>
	                                
	                <li class="row">

	                    <div class="col-xs-12">

	                        <label for="address_1" class="required">Ime i prezime</label>
	                        <input class="input form-control" name="address_1" id="address_1" type="text">

	                    </div>

	                </li>
	                <li class="row">

	                    <div class="col-xs-12">

	                        <label for="address_1" class="required">Telefon</label>
	                        <input class="input form-control" name="address_1" id="address_1" type="text">

	                    </div>

	                </li>
	                <li class="row">

	                    <div class="col-xs-12">

	                        <label for="address_1" class="required">E-mail</label>
	                        <input class="input form-control" name="address_1" id="address_1" type="text">

	                    </div>

	                </li>
	                <li class="row">

	                    <div class="col-xs-12">

	                        <label for="address_1" class="required">Adresa</label>
	                        <input class="input form-control" name="address_1" id="address_1" type="text">

	                    </div>

	                </li>
	              

	                <li class="row">

	                    <div class="col-sm-6">

	                        <label for="telephone_1" class="required">Grad</label>
	                        <input class="input form-control" name="telephone_1" id="telephone_1" type="text">

	                    </div>

	                    <div class="col-sm-6">

	                        <label for="fax_1">Poštanski broj</label>
	                        <input class="input form-control" name="fax_1" id="fax_1" type="text">

	                    </div>

	                </li>

	               

	            </ul>
	            <br>
	            <div class="actions">
                                                
                    <a style="background-color: #184376; color: white; " href="/korpa" class="btn btn-checkout" type="button" title="Check Out">
                        <i class="fa fa-check"></i> <span> Poruči</span>
                    </a>
                </div>
	        </div>
        </div>
    </div>

</div>
@stop