@extends('admin.adminLayout')

@section('title')
Izveštaji
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<span class="breadcrumb-item active">Izveštaji</span>
@stop

@section('heder-h1')
Izveštaji
@stop


@section('heder-h2')
Izaberite <a class="text-primary-light link-effect">vrstu izveštaja</a>.
@stop

@section('scriptsTop')
<link rel="stylesheet" href="{{asset('assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/plugins/select2/select2-bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css')}}">
<link rel="stylesheet" href="{{asset('css/izvestaji.css')}}"/>
@stop

@section('scriptsBottom')
<!-- Page JS Code -->
<script src="{{asset('/js/tabelaIzvestaji.js')}}"></script>
<!-- Page JS Code -->

<script src="{{asset('assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/masked-inputs/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/js/pages/be_forms_plugins.js')}}"></script>
<script src="{{asset('js/FileSaver.js')}}"></script>
<script src="{{asset('js/xlsx.full.min.js')}}"></script>
<script src="{{asset('js/tableexport.js')}}"></script>
<script>

    jQuery(function () {
        // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>
@endsection

@section('main')
<h2 class="content-heading">Izveštaji</h2>

<div class="block">
    <form action="/admin/izvestaj" method="GET">
    <div class="block-header block-header-default">
        <h3 class="block-title">Izaberite vrstu izveštaja, period i vremenski interval</h3>
        <div class="block-options">
            
        </div>
    </div>
    <div class="block-content">
        <div class="row items-push">
            <!-- Datepicker (Bootstrap forms) -->
            <div class="col-xl-6">    
                <div class="form-group row">
                    <label class="col-12" for="example-daterange1">Izaberite period</label>
                    <div class="col-lg-8">
                        <div  class="input-daterange input-group" data-date-format="dd.mm.yyyy." data-week-start="1" data-autoclose="true" data-today-highlight="true">
                            <input type="text" class="form-control" id="example-daterange1" name="datumOd" data-week-start="1" data-autoclose="true" data-today-highlight="true" @if(isset($datumOd)) value="{{date("d.m.Y.", strtotime($datumOd))}}" @else value="{{date('d.m.Y.', time())}}" @endif>
                            <div class="input-group-prepend input-group-append">
                                <span class="input-group-text font-w600">do</span>
                            </div>
                            <input type="text" class="form-control" id="example-daterange2" name="datumDo" data-week-start="1" data-autoclose="true" data-today-highlight="true" @if(isset($datumDo)) value="{{date("d.m.Y.", strtotime($datumDo))}}" @else value="{{date('d.m.Y.', time())}}" @endif>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-xl-6">
                <div class="form-group row">
                    <label class="col-12" for="example-daterange1">Izaberite vremenski interval</label>
                    <div class="col-md-9">
                        <select class="form-control" id="example-select" name="interval">
                            <option value="dan" @if(!isset($interval) or $interval == 'dan') selected @endif>Dan</option>
                            <option value="nedelja" @if(isset($interval) and $interval == 'nedelja') selected @endif>Nedelja</option>
                            <option value="mesec" @if(isset($interval) and $interval == 'mesec') selected @endif>Mesec</option>
                            <option value="godina" @if(isset($interval) and $interval == 'godina') selected @endif>Godina</option>
                        </select>
                    </div>
                </div>  
            
            </div>
            <!-- END Datepicker (Bootstrap forms) -->

            
        </div>
        <div class="container">
            <div class="form-group row">
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="proizvodi" value="proizvodi" @if(!isset($tipIzvestaja) or $tipIzvestaja == 'proizvodi') checked @endif>
                    <label class="custom-control-label" for="proizvodi">Izveštaj po proizvodima</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="gradovi" value="gradovi" @if(isset($tipIzvestaja) and $tipIzvestaja == 'gradovi') checked @endif>
                    <label class="custom-control-label" for="gradovi">Izveštaj po gradovima</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="brendovi" value="brendovi" @if(isset($tipIzvestaja) and $tipIzvestaja == 'brendovi') checked @endif>
                    <label class="custom-control-label" for="brendovi">Izveštaj po brendovima</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="dobavljači" value="dobavljači" @if(isset($tipIzvestaja) and $tipIzvestaja == 'dobavljači') checked @endif>
                    <label class="custom-control-label" for="dobavljači">Izveštaj po dobavljačima</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="kategorije" value="kategorija" @if(isset($tipIzvestaja) and $tipIzvestaja == 'kategorija') checked @endif>
                    <label class="custom-control-label" for="kategorije">Izveštaj po kategorijama</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="kupci" value="kupci" @if(isset($tipIzvestaja) and $tipIzvestaja == 'kupci') checked @endif>
                    <label class="custom-control-label" for="kupci">Izveštaj po kupcima</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="stavke" value="stavka" @if(isset($tipIzvestaja) and $tipIzvestaja == 'stavka') checked @endif>
                    <label class="custom-control-label" for="stavke">Izveštaj po stavkama</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="kuponi" value="kuponi" @if(isset($tipIzvestaja) and $tipIzvestaja == 'kuponi') checked @endif>
                    <label class="custom-control-label" for="kuponi">Izveštaj po kuponima</label>
                </div>
                <div class="col-4 custom-control custom-radio mb-5">
                    <input class="custom-control-input" type="radio" name="tipIzvestaja" id="vaučeri" value="vaučeri" @if(isset($tipIzvestaja) and $tipIzvestaja == 'vaučeri') checked @endif>
                    <label class="custom-control-label" for="vaučeri">Izveštaj po vaučerima</label>
                </div>
        </div>
        </div>
        <br/>
        <input class="btn btn-primary offset-5" type="submit" value="Prikaži izveštaj" style="font-size:15px; "/>

    </div>
    <br/>
    </form>
</div>
@if(isset($periodi))
    <!-- END Bootstrap Datepicker -->
<div class="row gutters-tiny">
    <!-- Out of Stock -->
    <div class="col-md-6 col-xl-2">
        <a class="block block-rounded block-link-shadow" style="cursor:pointer" onclick="javascript: $('.csv').remove(); $('#tabela-{{$tipIzvestaja}}').tableExport({formats:['csv']}); $('.csv').click();">
            <div class="block-content block-content-full block-sticky-options">
                <div class="text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-file-excel-o"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted" >Prebaci u EXCEL</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Out of Stock -->
    <!-- Add Product -->

    <!-- END Add Product -->
</div>

<!-- Dynamic Table Full Pagination -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Izveštaj po {{$tipIzvestaja}}ma za period {{date("d.m.Y.", strtotime($datumOd))}} - {{date("d.m.Y.", strtotime($datumDo))}}<small>  (interval - {{$interval}})</small></h3>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
        @if($tipIzvestaja == 'proizvodi' or $tipIzvestaja == 'gradovi' or $tipIzvestaja == 'brendovi' or $tipIzvestaja == 'dobavljači' or $tipIzvestaja == 'kategorija')
            <table id="tabela-{{$tipIzvestaja}}" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                    <tr>
                        <th class="text-center">Period</th>
                        <th>Naziv</th>
                        <th class="d-none d-sm-table-cell text-center">Porudžbina</th>
                        <th class="d-none d-sm-table-cell text-center">Komada</th>
                        <th >Razlika u ceni</th>
                        <th >Ukupno</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($periodi as $period)
                        @foreach($period->izvestaji as $izvestaj)
                        <tr>
                            <td class="text-center">{{$period->datum1}} @if($interval != 'dan')&emsp;|&emsp;{{$period->datum2}} @endif</td>
                            <td class="font-w600">{{$izvestaj->objekat}}</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->porudzbina}}</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->komada}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->razlika, 2, ',', '.')}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->ukupno, 2, ',', '.')}}</td>
                        </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        @elseif($tipIzvestaja == 'kupci')
            <table id="tabela-{{$tipIzvestaja}}"  class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                <tr>
                    <th style="width:20%;" class="text-center">Interval</th>
                    <th>Email</th>
                    <th>Kupac</th>
                    <th>Grad</th>
                    <th>Telefon</th>
                    <th class="d-none d-sm-table-cell text-center">Porudžbina</th>
                    <th class="d-none d-sm-table-cell text-center">Komada</th>
                    <th >Razlika u ceni</th>
                    <th >Ukupno</th>
                </tr>
                </thead>
                <tbody>
                @foreach($periodi as $period)
                    @foreach($period->izvestaji as $izvestaj)
                        <tr>
                            <td class="text-center">{{$period->datum1}} @if($interval != 'dan')&emsp;|&emsp;{{$period->datum2}} @endif</td>
                            <td class="font-w600">{{$izvestaj->objekat->email}}</td>
                            <td class="font-w600">{{$izvestaj->objekat->kupac}}</td>
                            <td class="font-w600">{{$izvestaj->objekat->grad}}</td>
                            <td class="font-w600">{{$izvestaj->objekat->telefon}}</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->porudzbina}}</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->komada}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->razlika, 2, ',', '.')}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->ukupno, 2, ',', '.')}}</td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        @elseif($tipIzvestaja == 'stavka')
            <table id="tabela-{{$tipIzvestaja}}"  class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                <tr>
                    <th class="text-center">Datum</th>
                    <th>Proizvod</th>
                    <th>Kupac</th>
                    <th>Grad</th>
                    <th class="d-none d-sm-table-cell">Kupon</th>
                    <th class="d-none d-sm-table-cell">Komada</th>
                    <th >Razlika u ceni</th>
                    <th >Ukupno</th>
                </tr>
                </thead>
                <tbody>
                @foreach($periodi as $period)
                    @foreach($period->izvestaji as $izvestaj)
                        <tr>
                            <td class="text-center">{{$izvestaj->objekat->porudzbina->created_at}}</td>
                            <td class="font-w600">{{$izvestaj->objekat->proizvod->naziv}}</td>
                            <td class="font-w600">{{$izvestaj->objekat->porudzbina->kupac}}</td>
                            <td class="font-w600">{{$izvestaj->objekat->porudzbina->grad}}</td>
                            <td class="d-none d-sm-table-cell">@if($izvestaj->objekat->id_kupon != null) {{$izvestaj->objekat->kupon->naziv}} ({{$izvestaj->objekat->kupon->popust*100}}%) @else - @endif</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->komada}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->razlika, 2, ',', '.')}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->ukupno, 2, ',', '.')}}</td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        @elseif($tipIzvestaja == 'kuponi')
            <table id="tabela-{{$tipIzvestaja}}"  class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                <tr>
                    <th style="width:20%;" class="text-center">Interval</th>
                    <th class="d-none d-sm-table-cell">Kupon</th>
                    <th class="d-none d-sm-table-cell text-center">Popust</th>
                    <th class="d-none d-sm-table-cell text-center">Porudžbina</th>
                    <th class="d-none d-sm-table-cell text-center">Broj korišćenja</th>
                    <th>Razlika u ceni</th>
                    <th>Ušteđeno</th>
                    <th>Ukupno</th>
                </tr>
                </thead>
                <tbody>
                @foreach($periodi as $period)
                    @foreach($period->izvestaji as $izvestaj)
                        <tr>
                            <td class="text-center">{{$period->datum1}} @if($interval != 'dan')&emsp;|&emsp;{{$period->datum2}} @endif</td>
                            <td class="font-w600">@if($izvestaj->objekat != null){{$izvestaj->objekat->naziv}} @else Nema kupona @endif</td>
                            <td class="d-none d-sm-table-cell text-center">@if($izvestaj->objekat != null) {{$izvestaj->objekat->popust*100}}% @else - @endif</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->porudzbina}}</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->komada}}</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->razlika, 2, ',', '.')}}</td>
                            <td class="d-none d-sm-table-cell">@if($izvestaj->objekat != null) {{number_format($izvestaj->objekat->ukupno_ustedjeno, 2, ',', '.')}} @else - @endif</td>
                            <td class="d-none d-sm-table-cell">{{number_format($izvestaj->ukupno, 2, ',', '.')}}</td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        @elseif($tipIzvestaja == 'vaučeri')
            <table id="tabela-{{$tipIzvestaja}}" class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
                <thead>
                <tr>
                    <th style="width:20%;" class="text-center">Interval</th>
                    <th class="d-none d-sm-table-cell">Vaučer</th>
                    <th class="d-none d-sm-table-cell">Iznos</th>
                    <th class="d-none d-sm-table-cell text-center">Porudžbina</th>
                    <th>Ušteđeno</th>
                </tr>
                </thead>
                <tbody>
                @foreach($periodi as $period)
                    @foreach($period->izvestaji as $izvestaj)
                        <tr>
                            <td class="text-center">{{$period->datum1}} @if($interval != 'dan')&emsp;|&emsp;{{$period->datum2}} @endif</td>
                            <td class="font-w600">@if($izvestaj->objekat != null){{$izvestaj->objekat->naziv}} @else Nema vaučera @endif</td>
                            <td class="d-none d-sm-table-cell">@if($izvestaj->objekat != null) {{$izvestaj->objekat->iznos}} @else - @endif</td>
                            <td class="d-none d-sm-table-cell text-center">{{$izvestaj->porudzbina}}</td>
                            <td class="d-none d-sm-table-cell">@if($izvestaj->objekat != null) {{number_format($izvestaj->objekat->ukupno_ustedjeno, 2, ',', '.')}} @else - @endif</td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
</div>
@endif
<!-- END Dynamic Table Full Pagination -->
@stop