@extends('admin.adminLayout')

@section('title')
Kupon - @if($izmena) {{$kupon->naziv}} @else Novi kupon @endif
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/kuponi">Kuponi</a>
<span class="breadcrumb-item active">@if($izmena) {{$kupon->naziv}} @else Novi kupon @endif</span>
@stop

@section('heder-h1')
@if($izmena)Kupon - {{$kupon->naziv}} @else Novi kupon @endif
@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/bootstrap-treeview.css')}}">
    <link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
    <script src="{{asset('js/adminKupon.js')}}"></script>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/bootstrap-treeview.js')}}"></script>
    <script src="{{asset('js/kategorija.js')}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>

    <!-- Da se ne bi pojavljivali &quot karateri radimo laravel ispis sa !! -->
    <script>
        $('.js-example-basic-multiple').select2();

        sakrijSelectove();

        @if(!$izmena or $kupon->za_sve_kategorije)
        inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}');
        @else
        inicijalizujKategorije('{!! addslashes(json_encode($stabloKategorija)) !!}', '{!! addslashes(json_encode($kupon->kategorije)) !!}');
        postaviPolja('{!! addslashes(json_encode($kupon)) !!}');
        @endif
    </script>
@endsection


@section('main')
<div class="row gutters-tiny">
@if($izmena)

    @if(!$kupon->sakriven)
         @if($kupon->aktivan)

        <!-- Delete Product -->
            <div class="col-xl-4">
                <form id="forma-deaktiviraj" action="/admin/deaktivirajKupon/{{$kupon->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-deaktiviraj').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-tag fa-2x text-danger-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-danger">
                                    <i class="fa fa-power-off"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Deaktiviraj kupon</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
        @else
            @if(!$kupon->ogranicen or $kupon->broj_koriscenja < $kupon->max_koriscenja)
            <div class="col-md-6 col-xl-4">
                <form id="forma-aktiviraj" action="/admin/aktivirajKupon/{{$kupon->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-aktiviraj').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-tag fa-2x text-success-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-success">
                                    <i class="fa fa-lightbulb-o"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Aktiviraj kupon</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
            @endif
        @endif

    @endif



    @if(!$kupon->sakriven)
        <!-- Delete Product -->
            <div class="col-xl-4">
                <form id="forma-obrisi" action="/admin/obrisiKupon/{{$kupon->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-obrisi').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-archive fa-2x text-danger-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-danger">
                                    <i class="fa fa-times"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši kupon</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
        @else
            <div class="col-md-6 col-xl-4">
                <form id="forma-restauriraj" action="/admin/restaurirajKupon/{{$kupon->id}}" method="POST">
                    {{csrf_field()}}
                    <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj').submit()">
                        <div class="block-content block-content-full block-sticky-options">
                            <div class="block-options">
                                <div class="block-options-item">
                                    <i class="fa fa-shopping-cart fa-2x text-warning-light"></i>
                                </div>
                            </div>
                            <div class="py-20 text-center">
                                <div class="font-size-h2 font-w700 mb-0 text-warning">
                                    <i class="fa fa-undo"></i>
                                </div>
                                <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj kupon</div>
                            </div>
                        </div>
                    </a>
                </form>
            </div>
    @endif

    <!-- END Delete Product -->


@endif
<!-- Stock -->
<div class="col-md-6 col-xl-4">
    <a class="block block-rounded block-link-shadow" href="javascript: document.getElementById('forma-kuponi-submit-button').click()">
        <div class="block-content block-content-full block-sticky-options">
            <div class="block-options">
                <div class="block-options-item">
                    <i class="si si-settings fa-2x text-success"></i>
                </div>
            </div>
            <div class="py-20 text-center">
                <div class="font-size-h2 font-w700 mb-0 text-success">
                    <i class="fa fa-check"></i>
                </div>
                <div class="font-size-sm font-w600 text-uppercase ">Sačuvaj</div>
            </div>
        </div>
    </a>
</div>

</div>

@if($errors->has('greska'))
    <p class="offset-4 col-7" style="color:red; font-weight:bold;">{{$errors->first('greska')}}</p>
@endif

<!-- END Overview -->
<form id="forma-kupon" @if(!$izmena) action="/admin/sacuvajKupon/-1" @else action="/admin/sacuvajKupon/{{$kupon->id}}" @endif method="POST" onsubmit="return kuponFormaSubmit()">
{{csrf_field()}}
<div class="row gutters-tiny">
    <!-- Basic Info -->
    <div class="col-md-6">

            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">KUPON</h3>
                </div>
                <div class="block-content block-content-full">
                	<div class="form-group row">
                        <label class="col-12">Naziv</label>
                        <div class="col-12">
                            <input type="text" maxlength="254" class="form-control" name="naziv" @if($izmena) value="{{$kupon->naziv}}" @endif required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12">Kod kupona (kod za ostvarivanje popusta)</label>
                        <div class="col-12">
                            <input type="text" maxlength="254" class="form-control" name="kod" @if($izmena) value="{{$kupon->kod}}" @endif required>
                        </div>
                    </div>


                        <div class="form-group row">
                            <label class="col-12">Popust uz kupon u procentima:</label>

                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-fw fa-percent"></i>
                                    </span>
                                    </div>
                                    <input type="number" min="1" max="100" class="form-control" name="popust" placeholder="20" @if($izmena) readonly @endif required>

                                </div>
                            </div>


                        </div>
                        <br>
                        <div class="form-group row">
                            <br>
                            <div class="col-12">
                                <br>
                                <div class="custom-control custom-checkbox custom-control-inline mb-5">

                                    <input onchange="ogranicenChanged(this)" class="custom-control-input" type="checkbox" name="ogranicen" id="ogranicenCB" value="option1" @if($izmena and $kupon->ogranicen) checked @endif>
                                    <label class="custom-control-label" for="ogranicenCB">Broj korišćenja kupona je ograničen?</label>
                                </div>

                            </div>
                            <label class="col-6" >Maksimalan broj korišćenja:</label>
                            <label class="col-6" >Iskorišćen:</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        MAX
                                    </span>
                                    </div>
                                    <input type="number" min="1" class="form-control" id="max-koriscenja" name="max_koriscenja" @if($izmena and $kupon->ogranicen) value="{{$kupon->max_koriscenja}}" @else readonly value="1" @endif required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="si fa-fw si-tag"></i>
                                    </span>
                                    </div>
                                    <input id="broj-koriscenja" type="number" @if($izmena) min={{$kupon->broj_koriscenja}} @endif class="form-control"  name="broj_koriscenja" @if($izmena) value="{{$kupon->broj_koriscenja}}" @else value="0" readonly @endif required>
                                </div>

                            </div>
                            @if($izmena)
                            <div class="col-sm-12">
                                <br/><br/>
                                <label> Status:&emsp; <span @if($kupon->aktivan) class="text-success" @else class="text-danger" @endif> @if($kupon->aktivan) Aktivan @else Neaktivan @endif</span></label>
                            </div>
                            @endif

                        </div>


                </div>
            </div>

            <div id="kupci-kupon" class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">KUPCI ZA KOJE VAŽI KUPON:</h3>
                </div>
                <div class="block-content block-content-full row">
                    <select id="korisnici" class="js-example-basic-multiple col-12" name="korisnici[]" multiple="multiple">
                        @foreach($korisnici as $korisnik)
                            <option value="{{$korisnik->id}}">{{$korisnik->ime_prezime}}&emsp;({{$korisnik->email}})</option>
                        @endforeach
                    </select>
                    
                </div>
            </div>

            <div id="proizvodi-kupon" class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">DODATNI PROIZVODI ZA KOJE VAŽI KUPON:</h3>
                </div>
                <div class="block-content block-content-full row">
                    <select id="proizvodi" class="js-example-basic-multiple col-12" name="proizvodi[]" multiple="multiple">
                        @foreach($proizvodi as $proizvod)
                            <option value="{{$proizvod->id}}">{{$proizvod->naziv}}</option>
                        @endforeach
                    </select>

                    
                </div>
            </div>
    </div>

    <!-- END Basic Info -->
    
    <!-- More Options -->
    <div class="col-md-6">
        <!-- Status -->
            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">OGRANIČENJA</h3>
                    
                </div>
                <div class="block-content block-content-full">

                    <div id="kategorije-blok" class="form-group row">
                        <label class="col-12" >Kategorije:</label>
                        <div class="col-12">
                            <label class="css-control css-control-primary css-radio">
                                <input onchange="kategorijeChanged(1)" type="radio" class="css-control-input"  name="za_sve_kategorije" value="1" @if(!$izmena or $kupon->za_sve_kategorije) checked @endif>
                                <span class="css-control-indicator"></span> Važi za sve kategorije
                            </label>
                            <label class="css-control css-control-secondary css-radio">
                                <input onchange="kategorijeChanged(0)"type="radio" class="css-control-input" name="za_sve_kategorije" value="0" @if($izmena and !$kupon->za_sve_kategorije) checked @endif>
                                <span class="css-control-indicator"></span> Važi za obeležene kategorije
                            </label>

                            <div style="display: none" id="kategorije-tree" class="form-group row">
                                <div class="col-12">
                                    <div class="block-content block-content-full row">
                                        <div class="col-sm-12">
                                            <div id="treeview-selectable"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="snizenje-blok" class="form-group row">
                        <label class="col-12" >Sniženje:</label>
                        <div class="col-12">
                            <label class="css-control css-control-primary css-radio">
                                <input type="radio" class="css-control-input" name="za_snizene" value="1" @if(!$izmena or $kupon->za_snizene) checked @endif>
                                <span class="css-control-indicator"></span> Važi za sve proizvode iz odabranih kategorija
                            </label>
                            <br/>
                            <label class="css-control css-control-secondary css-radio">
                                <input type="radio" class="css-control-input"  name="za_snizene" value="0" @if($izmena and !$kupon->za_snizene) checked @endif>
                                <span class="css-control-indicator"></span> Ne važi za proizvode na sniženju iz odabranih kategorija
                            </label>
                        </div>
                    </div>

                    <div id="kupci-blok" class="form-group row">
                        <label class="col-12" >Kupci:</label>
                        <div class="col-12">
                            <label class="css-control css-control-primary css-radio">
                                <input onchange="zaSveKupceChanged(1)" type="radio" class="css-control-input"  name="za_sve_kupce" value="1" @if(!$izmena or $kupon->za_sve_kupce) checked @endif>
                                <span class="css-control-indicator"></span> Važi za sve kupce
                            </label>
                            <label class="css-control css-control-secondary css-radio">
                                <input onchange="zaSveKupceChanged(0)" type="radio" class="css-control-input" name="za_sve_kupce" value="0" @if($izmena and !$kupon->za_sve_kupce) checked @endif>
                                <span class="css-control-indicator"></span> Važi za odabrane kupce
                            </label>
                        </div>
                    </div>


                    <div style="display: none" id="proizvodi-blok">
                        <br/>
                        <div class="col-12 custom-control custom-checkbox custom-control-inline mb-5">

                            <input onchange="josProizvodaChanged(this)" class="custom-control-input" type="checkbox" name="jos_proizvoda" id="jos_proizvodaCB" value="1" @if($izmena and !$kupon->za_sve_kategorije and $kupon->jos_proizvoda) checked @endif>
                            <label class="custom-control-label" for="jos_proizvodaCB">Dodati pojedinačne proizvode van odabranih kategorija, za koje kupon važi?</label>
                        </div>
                    </div>

                </div>

            </div>

    </div>
        <!-- END Status -->
</div>
    <!-- END More Options -->
<!-- END Update Product -->
    <input type="submit" style="display:none" id="forma-kuponi-submit-button"/>
</form>
@stop