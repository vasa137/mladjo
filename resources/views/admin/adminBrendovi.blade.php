@extends('admin.adminLayout')

@section('title')
    Brendovi
@stop

@section('heder-h1')
Brendovi
@stop

@section('heder-h2')
Trenutno <a class="text-primary-light link-effect" href="#">{{count($aktivniBrendovi)}} aktivnih brendova</a>.
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">Brendovi</span>
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminBrendovi.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaBrendovi.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($aktivniBrendovi) + count($obrisaniBrendovi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno brendova</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziDostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($aktivniBrendovi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Aktivnih</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziNedostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisaniBrendovi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obrisanih</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Out of Stock -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/brend/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novi brend</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 id="brendovi-title" class="block-title">Brendovi</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table id="tabela-brendovi-aktivni" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th style="width:40%;">Naziv</th>
                        <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih proizvoda</th>
                        <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih kategorija</th>
                        <th class="text-center" style="width:20%;">Akcija</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($aktivniBrendovi as $brend)
                    <tr>
                        <td class="font-w600">{{$brend->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$brend->broj_proizvoda}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$brend->broj_kategorija}}</td>

                        <td class="text-center">
                            <a href="/admin/brend/{{$brend->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni brend">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/obrisiBrend/{{$brend->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Obriši brend">
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <table id="tabela-brendovi-obrisani" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none;">
                <thead>
                <tr>
                    <th style="width:40%;">Naziv</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih proizvoda</th>
                    <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih kategorija</th>
                    <th class="text-center" style="width:20%;">Akcija</th>
                </tr>
                </thead>
                <tbody>
                @foreach($obrisaniBrendovi as $brend)
                    <tr>
                        <td class="font-w600">{{$brend->naziv}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$brend->broj_proizvoda}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$brend->broj_kategorija}}</td>

                        <td class="text-center">
                            <a href="/admin/brend/{{$brend->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni brend">
                                <i class="fa fa-edit"></i>
                            </a>

                            <form method="POST" action="/admin/restaurirajBrend/{{$brend->id}}" style="display:inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj brend">
                                    <i class="fa fa-undo"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@stop