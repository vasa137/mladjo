@foreach($stabloKategorija as $kategorija)
<!-- Filter Item  categori-->
    @if($kategorija->broj_proizvoda > 0)
    <div class="filter-options-item filter-options-categori">
            <input type="hidden" name="glavne[]" value="{{$kategorija->id}}"/>
            @if(empty($kategorija->children) or ($checkboxZaGlavneOdvojene and in_array($kategorija->id, $glavneOdvojene)))
                <!-- <input type="hidden" name="glavneNeraspakovane[]" value="{{$kategorija->id}}"/> -->
                <div class="filter-options-content">
                    <ol class="items">
                        <li class="item ">
                            <label>
                                <input @if(in_array($kategorija->id, $izabraneKategorije)) checked @endif onchange="filtriraj();" type="checkbox" name="kategorije[{{$kategorija->id}}][]" value="{{$kategorija->id}}"><span><span class="filter-options-title" style="display: inline;">{{$kategorija->naziv}}</span> <span class="count">({{$kategorija->broj_proizvoda}})</span></span>
                            </label>
                        </li>
                    </ol>
                </div>
            @else
                 <div class="filter-options-title">{{$kategorija->naziv}} <!--<span class="count">({{$kategorija->broj_proizvoda}})</span>--></div>
            @endif

            @if(!empty($kategorija->children))
            <div class="filter-options-content">
                <ol class="items">
                    @foreach($kategorija->children as $child)
                        @if($child->broj_proizvoda > 0)
                            <li class="item ">
                                <label>
                                    <input @if(in_array($child->id, $izabraneKategorije)) checked @endif onchange="filtriraj();" type="checkbox" name="kategorije[{{$kategorija->id}}][]" value="{{$child->id}}"><span>{{$child->naziv}} <span class="count">({{$child->broj_proizvoda}})</span></span>
                                </label>
                            </li>
                         @endif
                    @endforeach
                </ol>
            </div>
            @endif

    </div><!-- Filter Item  categori-->
    @endif
@endforeach

<!-- filter brad-->
@if($ukupnoProizvodaBrendovi > 0)
<div class="filter-options-item filter-options-brand">
    <div class="filter-options-title">Brendovi</div>
    <div class="filter-options-content">
        <ol class="items">
            @foreach($brendovi as $brend)
                @if($brend->broj_proizvoda > 0)
                    <li class="item ">
                        <label>
                            <input @if(in_array($brend->id, $izabraniBrendovi)) checked @endif onchange="filtriraj();" type="checkbox" name="brendovi[]" value="{{$brend->id}}"><span>{{$brend->naziv}} <span class="count">({{$brend->broj_proizvoda}})</span>  </span>
                        </label>
                    </li>
                @endif
            @endforeach
        </ol>
    </div>
</div><!-- Filter Item -->
@endif
