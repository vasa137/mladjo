@foreach($proizvodi as $proizvod)
<li class="col-sm-3 product-item ">
    <div class="product-item-opt-1">
        <div class="product-item-info">
            <div class="product-item-photo">
                @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
                    <a href="/proizvod/{{$proizvod->link}}/{{$proizvod->id}}" class="product-item-img">
                        <img src="images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="{{$proizvod->nazivGlavneSlike}}">
                    </a>
                @endif
                <a href="/proizvod/{{$proizvod->link}}/{{$proizvod->id}}" class="btn btn-cart" type="button"><span>Pogledaj detalje</span></a>
                <!-- <span class="product-item-label label-price">30% <span>off</span></span> -->
            </div>
            <div class="product-item-detail">
                <strong class="product-item-name"><a href="/proizvod/{{$proizvod->link}}/{{$proizvod->id}}">@if($proizvod->id_brend != null) {{$proizvod->brend->naziv}} @endif {{$proizvod->naziv}}</a></strong>
                <div class="clearfix">
                    <div class="product-item-price">
                        @if($proizvod->na_popustu)
                            <span class="price">{{number_format($proizvod->cena_popust, 2, ',', '.')}} rsd</span>
                            <del class="old-price">{{number_format($proizvod->cena, 2, ',', '.')}} rsd</del>
                        @else
                            <span class="price">{{number_format($proizvod->cena, 2, ',', '.')}} rsd</span>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
</li>
@endforeach



