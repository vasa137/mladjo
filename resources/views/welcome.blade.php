@extends('layout')

@section('body')
class="cms-index-index index-opt-1"
@stop

@section('title')

@stop

@section('main')
<div class="block-section-top block-section-top1">
    <div class="container">
        <div class="col-md-9 col-sm-12" style="height: 430px;">
                <img src="https://www.oddoigracke.rs/banerPics/39/slagalice-puzzle-oddo-igracke.jpg" height="430" width="100%">
            
        </div>
        
        <div class="col-md-3 col-sm-12" style="height: 370px;">

            <!-- block deals  of -->
            <div class="block-deals-of block-deals-of-opt1" >
                <div class="block-title ">
                    <span class="icon"></span>
                    <div class="heading-title">POŽURITE!</div>
                </div>
                <div class="block-content">
                    
                    <div class="owl-carousel" 
                        data-nav="true"
                        data-dots="false" 
                        data-margin="30" 
                        data-responsive='{
                        "0":{"items":1},
                        "480":{"items":2},
                        "768":{"items":3},
                        "992":{"items":1},
                        "1200":{"items":1}
                        }'>
                        
                        <div class="product-item  product-item-opt-1 ">
                            <div class="deals-of-countdown">
                                
                                <div class="count-down-time" data-countdown="2016/12/25"></div>
                            </div>
                            <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="product-item  product-item-opt-1 ">
                            <div class="deals-of-countdown">
                                
                                <div class="count-down-time" data-countdown="2016/11/25"></div>
                            </div>
                            <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>    
                                
                </div>
            </div><!-- block deals  of -->
        </div>
    </div>
    
</div>

<!-- block  service-->
<div class="container ">
    <div class="block-service-opt1">
        <div class="clearfix">
            <div class="col-md-4 col-sm-6">
                <div class="item">
                    <span class="icon">
                        <img src="images/media/index1/besplatna-dostava.png" alt="service">
                    </span>
                    <strong class="title">besplatna dostava </strong>
                    <span>Za iznose preko 3000 din</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="item">
                    <span class="icon">
                        <img src="images/media/index1/service2.png" alt="service">
                    </span>
                    <strong class="title">Vraćamo novac!</strong>
                    <span>Moneyback guarantee</span>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6">
                <div class="item">
                    <span class="icon">
                        <img src="images/media/index1/service4.png" alt="service">
                    </span>
                    <strong class="title">SIGURNA KUPOVINA </strong>
                    <span>SSL Sertifikovana prodavnica</span>
                </div>
            </div>
            
        </div>
    </div>
</div><!-- block  service-->


<div class="container">
    <div class="row">

        <div class="col-md-12">
            
            <!-- block tab products -->
            <div class="block-tab-products-opt1">

                <div class="block-title">
                    <ul class="nav" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tabproduct1"  role="tab" data-toggle="tab">Najprodavaniji </a>
                        </li>
                        <li role="presentation">
                            <a href="#tabproduct2" role="tab" data-toggle="tab">Na sniženju</a>
                        </li>
                        <li role="presentation">
                            <a href="#tabproduct3"  role="tab" data-toggle="tab">Novo</a>
                        </li>
                    </ul>
                </div>

                <div class="block-content tab-content">

                    <!-- tab 1 -->
                    <div role="tabpanel" class="tab-pane active fade in " id="tabproduct1">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="30" 
                            data-responsive='{
                            "0":{"items":1},
                            "480":{"items":2},
                            "480":{"items":2},
                            "768":{"items":3},
                            "992":{"items":3}
                        }'>

                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                             
                            

                        </div>
                    </div><!-- tab 1 -->

                    <!-- tab 2 -->
                    <div role="tabpanel" class="tab-pane fade" id="tabproduct2">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="30" 
                            data-responsive='{
                            "0":{"items":1},
                            "480":{"items":2},
                            "480":{"items":2},
                            "768":{"items":3},
                            "992":{"items":3}
                        }'>

                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                            
                             
                            

                        </div>
                    </div><!-- tab 2 -->

                    <!-- tab 3-->
                    <div role="tabpanel" class="tab-pane fade" id="tabproduct3">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="30" 
                            data-responsive='{
                            "0":{"items":1},
                            "480":{"items":2},
                            "480":{"items":2},
                            "768":{"items":3},
                            "992":{"items":3}
                        }'>

                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/151412/17321.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div><!-- tab 3-->

                </div>

            </div><!-- block tab products -->

        </div>

    </div>
</div>



<div class="clearfix" style="background-color: #eeeeee;margin-bottom: 40px; padding-top:30px;">

    <!-- block -floor -products / floor 1 :Fashion-->
    <div class="block-floor-products block-floor-products-opt1 floor-products1" id="floor0-1">
        <div class="container">
            <div class="block-title ">
                <span class="title"><img alt="img"  src="images/media/index1/floor1.png">BRENDOVI</span>
                <div class="links dropdown">
                    <button class="dropdown-toggle"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" >
                        <ul  >
                            <li role="presentation" class="active"><a href="#floor1-1"  role="tab" data-toggle="tab">BREND 1 </a></li>
                            <li role="presentation"><a href="#floor1-2"  role="tab" data-toggle="tab">BREND 2 </a></li>
                            <li role="presentation"><a href="#floor1-3"  role="tab" data-toggle="tab">BREND 3 <span class="label-cat">12</span></a></li>
                            <li role="presentation"><a href="#floor1-4"  role="tab" data-toggle="tab">BREND  4 </a></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="actions">
                    <a href="#" class="action action-up"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                    <a href="#floor0-2" class="action action-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                </div>
            </div>

            <!-- Banner -->
            <div class="block-banner-floor" style="width: 100%">     
                    <a href="#" class="box-img"><img  src="images/akcija-mali.jpg" alt="banner"></a>
            </div><!-- Banner -->

            <div class="block-content">

                <div class="col-banner">
                    
                    <a href="#" class="box-img"><img src="images/sale1.jpg" alt="baner-floor"></a>
                </div>

                <div class="col-products tab-content">

                    <!-- tab 1 -->
                    <div class="tab-pane active in  fade " id="floor1-1" role="tabpanel">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="0" 
                            data-responsive='{
                            "0":{"items":1},
                            "420":{"items":2},
                            "600":{"items":3},
                            "768":{"items":3},
                            "992":{"items":3},
                            "1200":{"items":4}
                        }'>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- tab 2-->
                    <div class="tab-pane  fade" id="floor1-2" role="tabpanel">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="0" 
                            data-responsive='{
                            "0":{"items":1},
                            "420":{"items":2},
                            "600":{"items":3},
                            "768":{"items":3},
                            "992":{"items":3},
                            "1200":{"items":4}
                        }'>
                            
                           
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <!-- tab 3 -->
                    <div class="tab-pane  fade" id="floor1-3" role="tabpanel">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="0" 
                            data-responsive='{
                            "0":{"items":1},
                            "420":{"items":2},
                            "600":{"items":3},
                            "768":{"items":3},
                            "992":{"items":3},
                            "1200":{"items":4}
                        }'>
                            
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- tab 4 -->
                    <div class="tab-pane  fade" id="floor1-4" role="tabpanel">
                        <div class="owl-carousel" 
                            data-nav="true" 
                            data-dots="false" 
                            data-margin="0" 
                            data-responsive='{
                            "0":{"items":1},
                            "420":{"items":2},
                            "600":{"items":3},
                            "768":{"items":3},
                            "992":{"items":3},
                            "1200":{"items":4}
                        }'>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152379/5458-2.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item  product-item-opt-1 ">
                                <div class="product-item-info">
                                    <div class="product-item-photo">
                                        <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                                        <div class="product-item-actions">
                                           
                                            <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                                        </div>
                                        <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                                    </div>
                                    <div class="product-item-detail">
                                        <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                                        <div class="clearfix">
                                            <div class="product-item-price">
                                                <span class="price">2900</span>
                                                <span class="old-price">3700</span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div><!-- block -floor -products / floor :Fashion-->

   
    

    <!-- Banner -->
    <div class="block-banner-opt1 effect-banner3">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#" class="box-img"><img src="images/akcija1.jpg" alt="banner"></a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="box-img"><img src="images/akcija2.jpg" alt="banner"></a>
                </div>
            </div>
        </div>
    </div><!-- Banner -->

</div>




<div class="block-upsell container">
    <div class="block-title">
        <strong class="title">Pogledali ste ranije:</strong>
    </div>
    <div class="block-content ">
        <ol class="product-items owl-carousel " data-nav="true" data-dots="false" data-margin="30" data-responsive='{"0":{"items":1},"480":{"items":2},"600":{"items":3},"992":{"items":3},"1200":{"items":4}}'>
            
            
            <li class="product-item product-item-opt-2">
                <div class="product-item  product-item-opt-1 ">
                    <div class="product-item-info">
                        <div class="product-item-photo">
                            <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                            <div class="product-item-actions">
                               
                                <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                            </div>
                            <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                        </div>
                        <div class="product-item-detail">
                            <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                            <div class="clearfix">
                                <div class="product-item-price">
                                    <span class="price">2900</span>
                                    <span class="old-price">3700</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="product-item product-item-opt-2">
                <div class="product-item  product-item-opt-1 ">
                    <div class="product-item-info">
                        <div class="product-item-photo">
                            <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                            <div class="product-item-actions">
                               
                                <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                            </div>
                            <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                        </div>
                        <div class="product-item-detail">
                            <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                            <div class="clearfix">
                                <div class="product-item-price">
                                    <span class="price">2900</span>
                                    <span class="old-price">3700</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="product-item product-item-opt-2">
                <div class="product-item  product-item-opt-1 ">
                    <div class="product-item-info">
                        <div class="product-item-photo">
                            <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                            <div class="product-item-actions">
                               
                                <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                            </div>
                            <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                        </div>
                        <div class="product-item-detail">
                            <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                            <div class="clearfix">
                                <div class="product-item-price">
                                    <span class="price">2900</span>
                                    <span class="old-price">3700</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="product-item product-item-opt-2">
                <div class="product-item  product-item-opt-1 ">
                    <div class="product-item-info">
                        <div class="product-item-photo">
                            <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                            <div class="product-item-actions">
                               
                                <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                            </div>
                            <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                        </div>
                        <div class="product-item-detail">
                            <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                            <div class="clearfix">
                                <div class="product-item-price">
                                    <span class="price">2900</span>
                                    <span class="old-price">3700</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="product-item product-item-opt-2">
                <div class="product-item  product-item-opt-1 ">
                    <div class="product-item-info">
                        <div class="product-item-photo">
                            <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg"></a>
                            <div class="product-item-actions">
                               
                                <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                            </div>
                            <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                        </div>
                        <div class="product-item-detail">
                            <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                            <div class="clearfix">
                                <div class="product-item-price">
                                    <span class="price">2900</span>
                                    <span class="old-price">3700</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="product-item product-item-opt-2">
                <div class="product-item  product-item-opt-1 ">
                    <div class="product-item-info">
                        <div class="product-item-photo">
                            <a class="product-item-img" href="#"><img alt="product name" src="https://www.oddoigracke.rs/proizvodi/152336/5329-2.jpg"></a>
                            <div class="product-item-actions">
                               
                                <a class="btn btn-quickview" href="#"><span>Pregled</span></a>
                            </div>
                            <button type="button" class="btn btn-cart"><span>Kupi</span></button>
                        </div>
                        <div class="product-item-detail">
                            <strong class="product-item-name"><a href="#">Naziv Slagalice</a></strong>
                            <div class="clearfix">
                                <div class="product-item-price">
                                    <span class="price">2900</span>
                                    <span class="old-price">3700</span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            
            
        
        </ol>
    </div>
</div><!-- block-Upsell Products -->






<!-- block  hot categories
<div class="block-hot-categories-opt1">
    <div class="container">

        <div class="block-title ">
            <span class="title">Hot categories</span>
        </div>

        <div class="block-content">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        
                        <div class="description" style="background-image: url(images/media/index1/hot-categories1.png)">
                            <div class="title"><span>Electronics</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Batteries & Chargers</a></li>
                            <li><a href="#">Headphone, Headset</a></li>
                            <li><a href="#">Home Audio</a></li>
                            <li><a href="#">Mp3 Player Accessories</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories2.png)">
                            <div class="title"><span>Jewelry &  <br>Watches</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Men Watches</a></li>
                            <li><a href="#">Wedding Rings</a></li>
                            <li><a href="#">Earring</a></li>
                            <li><a href="#">Necklaces</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories3.png)">
                            <div class="title"><span>Sport &  <br>Outdoors</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Golf Supplies</a></li>
                            <li><a href="#">Outdoor & Traveling Supplies</a></li>
                            <li><a href="#">Camping & Hiking</a></li>
                            <li><a href="#">Fishing</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories4.png)">
                            <div class="title"><span>Digital</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Accessories for iPhone</a></li>
                            <li><a href="#">Accessories for iPad</a></li>
                            <li><a href="#">Accessories for Tablet PC</a></li>
                            <li><a href="#">Tablet PC</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories5.png)">
                            <div class="title"><span>Fashion</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Batteries & Chargers</a></li>
                            <li><a href="#">Headphone, Headset</a></li>
                            <li><a href="#">Home Audio</a></li>
                            <li><a href="#">Mp3 Player Accessories</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories6.png)">
                            <div class="title"><span>Furniture</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Batteries & Chargers </a></li>
                            <li><a href="#">Headphone, Headset</a></li>
                            <li><a href="#">Home Audio</a></li>
                            <li><a href="#">Mp3 Player Accessories</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories7.png)">
                            <div class="title"><span>Health & <br>Beauty</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Bath & Body</a></li>
                            <li><a href="#">Shaving & Hair Removal</a></li>
                            <li><a href="#">Fragrances</a></li>
                            <li><a href="#">Salon & Spa Equipment</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="description" style="background-image: url(images/media/index1/hot-categories8.png)">
                            <div class="title"><span>Toys &   <br>Hobbies</span></div>
                            <a href="#" class="btn">shop now</a>
                        </div>
                        <ul>
                            <li><a href="#">Walkera</a></li>
                            <li><a href="#">Fpv System & Parts</a></li>
                            <li><a href="#">RC Cars & Parts</a></li>
                            <li><a href="#">Helicopters & Part</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>-->





@stop