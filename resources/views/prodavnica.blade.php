@extends('layout')

@section('body')
class="index-opt-1 catalog-category-view catalog-view_op1"
@stop

@section('title')
Prodavnica - Puzzle za decu - 
@stop

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/klijentProizvodi.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/prodavnica.js')}}"></script>
@endsection


@section('main')


<div class="columns container">
    <!-- Block  Breadcrumb-->
    <div class="row">
        <!-- Main Content -->
        <div class="col-md-9 col-md-push-3  col-main">
                <!-- images categori -->
                <div class="category-view">
                    <div class="owl-carousel "
                         data-nav="true"
                         data-dots="false"
                         data-margin="0"
                         data-items='1'
                         data-autoplayTimeout="700"
                         data-autoplay="true"
                         data-loop="true">
                        <div class="item " >

                            <a href="#"><img src="images/akcija-mali.jpg" alt="category-images"></a>
                        </div>
                        
                    </div>
                </div><!-- images categori -->
                 <!-- Toolbar -->
                <div class=" toolbar-products toolbar-top">



                    <div class="block-title">
                        <strong>PROIZVODI</strong>
                    </div>
                    <div class="toolbar-option">

                        <form id="sortirajForma">
                            <div class="toolbar-sorter ">
                                <label    class="label">Sortiraj po:</label>
                                <select name="sort" class="sorter-options form-control" >
                                    <option hidden value="null">Izaberi kriterijum</option>
                                    <option value="cena-asc">Ceni rastuće</option>
                                    <option value="cena-desc">Ceni opadajuće</option>
                                    <option value="naziv-asc">Naziv rastuće</option>
                                    <option value="naziv-desc">Naziv opadajuće</option>
                                </select>
                                <a href="#" class="sorter-action"></a>
                            </div><!-- Short by -->
                        </form>

                    </div>
                </div><!-- Toolbar -->



                <!-- List Products -->
                <div class="products  products-grid">
                    <ol class="product-items row" id="proizvodiInclude">
                        @include('include.listaProizvoda', ['proizvodi' => $proizvodi])
                    </ol>
                    <div class="ajax-load" ></div>
                </div>
             </div><!-- Main Content -->

            <!-- Sidebar -->
            <div class="col-md-3 col-md-pull-9  col-sidebar">
                <!-- block filter products -->
                <div id="layered-filter-block" class="block-sidebar block-filter no-hide">
                    <form id="filterForma">
                        <div class="block-content" id="filteriInclude">
                            @include('include.filterTabla', ['stabloKategorija' => $stabloKategorija, 'izabraneKategorije' => $izabraneKategorije, 'brendovi' => $brendovi, 'izabraniBrendovi' => $izabraniBrendovi, 'checkboxZaGlavneOdvojene' => $checkboxZaGlavneOdvojene, 'glavneOdvojene' =>$glavneOdvojene])
                        </div>
                    </form>

                </div>

                <!-- block slide top -->
                <div class="block-sidebar block-banner-sidebar">
                    <div class="owl-carousel"
                         data-nav="false"
                         data-dots="true"
                         data-margin="0"
                         data-items='1'
                         data-autoplayTimeout="700"
                         data-autoplay="true"
                         data-loop="true">
                        <div class="item item1" >
                            <img src="images/sal22.jpg" alt="images">
                        </div>
                        <!--
                        <div class="item item2" >
                            <img src="images/media/banner-sidebar1.jpg" alt="images">
                        </div>
                        <div class="item item3" >
                            <img src="images/media/banner-sidebar1.jpg" alt="images">
                        </div>
                    -->
                    </div>
                </div><!-- block slide top -->
            </div>


    </div>
</div>




@stop