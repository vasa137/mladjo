<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')Puzzle Shop - jedina online prodavnica slagalica u Srbiji!</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

    @yield('scriptsTop')

</head>

<body @yield('body')>


    <div class="wrapper">


        <!-- SPECIJALNA PONUDA
        <div role="alert" class="qc-top-site qc-top-site1 alert  fade in" style="background-image: url(images/media/index1/bg-qc-top.jpg);"> 
            <div class="container">
                <button class="close" type="button"><span aria-hidden="true">×</span></button> 
                <div class="description">
                    <span class="title">Special Offer!</span>
                    <span class="subtitle">Rewarding all customers with a 15% discount. </span>
                    <span class="des">This offer is available from 9th December to 19th December 2015.</span>
                    
                </div>
            </div>
        </div>
    -->

        <!-- HEADER -->
        <header class="site-header header-opt-1 cate-show">

            <!-- header-top -->
            <div class="header-top">
                <div class="container">

                    <!-- nav-left -->
                    <ul class="nav-left" >
                        
                        <li >
                        <a href="mailto:office@oddoigracke.rs">
                            <span><i class="fa fa-envelope" aria-hidden="true"></i> office@oddoigracke.rs</span>
                        </a>
                        </li>
                        <li >
                        <a href="/kontakt">
                            <span><i class="fa fa-envelope" aria-hidden="true"></i> Kontaktirajte nas</span>
                        </a>
                        </li>
                        
                    </ul><!-- nav-left -->

                     

                    <ul class=" nav-right">
                        <li class="dropdown setting">
                            <img style="height: 25px;" src="{{asset('/images/facebook.png')}}">
                            <img style="height: 29px;" src="{{asset('/images/instagram.png')}}">
                            
                            
                        </li>
                        
                        
                    </ul> 

                </div>
            </div><!-- header-top -->

            <!-- header-content -->
            <div class="header-content">
                <div class="container">

                    <div class="row">

                        <div class="col-md-3 nav-left">

                            <!-- logo -->
                            <strong class="logo">
                                <a href="/"><img src="{{asset('images/logo.png')}}" alt="logo"></a>
                            </strong>

                        </div>

                        <div class="nav-right">  

                            <!-- block mini cart -->  
                            <div class="block-minicart dropdown">
                                <a class="dropdown-toggle" href="/korpa" role="button" data-toggle="dropdown">
                                    <span class="cart-icon"></span>
                                    <span class="counter qty">
                                        <span class="cart-text">Korpa</span>
                                        <span class="counter-number">6</span>
                                        <span class="counter-label">6 <span>Artikala</span></span>
                                        <span class="counter-price">8,900 rsd</span>
                                    </span>
                                </a>

                                <div class="dropdown-menu">
                                    <form>
                                        <div  class="minicart-content-wrapper" >
                                            <div class="subtitle">
                                                Imate 6 proizvoda u korpi
                                            </div>
                                            <div class="minicart-items-wrapper">
                                                <ol class="minicart-items">
                                                    <li class="product-item">
                                                        <a class="product-item-photo" href="#" title="The Name Product">
                                                            <img class="product-image-photo" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg" alt="The Name Product">
                                                        </a>
                                                        <div class="product-item-details">
                                                            <strong class="product-item-name">
                                                                <a href="#">Donec Ac Tempus</a>
                                                            </strong>
                                                            <div class="product-item-price">
                                                                <span class="price">1,000 rsd</span>
                                                            </div>
                                                            <div class="product-item-qty">
                                                                <span class="label">Količina: </span ><span class="number">1</span>
                                                            </div>
                                                            <div class="product-item-actions">
                                                                <a class="action delete" href="#" title="Remove item">
                                                                    <span>Ukloni</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="product-item">
                                                        <a class="product-item-photo" href="#" title="The Name Product">
                                                            <img class="product-image-photo" src="https://www.oddoigracke.rs/proizvodi/153186/4278-box.jpg" alt="The Name Product">
                                                        </a>
                                                        <div class="product-item-details">
                                                            <strong class="product-item-name">
                                                                <a href="#">Donec Ac Tempus</a>
                                                            </strong>
                                                            <div class="product-item-price">
                                                                <span class="price">3,200 rsd</span>
                                                            </div>
                                                            <div class="product-item-qty">
                                                                <span class="label">Količina: </span ><span class="number">1</span>
                                                            </div>
                                                            <div class="product-item-actions">
                                                                <a class="action delete" href="#" title="Remove item">
                                                                    <span>Ukloni</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </div>
                                            <div class="subtotal">
                                                <span class="label">Ukupno</span>
                                                <span class="price">12,320 rsd</span>
                                            </div>
                                            <div class="actions">
                                                
                                                <a href="/korpa" class="btn btn-checkout" type="button" title="Check Out">
                                                    <span>Poruči</span>
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                                
                             
                        
                        </div>

                        <div class="nav-mind">   

                            <!-- block search -->
                            <div class="block-search">
                                <div class="block-title">
                                    <span>Pretraga</span>
                                </div>
                                <div class="block-content">
                                    
                                    <div class="form-search">
                                        <form>
                                            <div class="box-group">
                                                <input type="text" class="form-control" placeholder="unesite pojam za pretragu...">
                                                <button class="btn btn-search" type="button"><span>Pretraga</span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="nav-mind" style="float: right; padding-left: 100px;">   
                                    <img align="left"style="height: 60px;" src="/images/dostava.png">
                                    <a href="tel:011 228 00 74">
                                        <img align="left"style="height: 30px;" src="/images/fiksni.png">
                                    </a>
                                    <a href="viber://chat?number=381646160673">
                                    <img style="height: 30px;" src="{{asset('/images/icon-viber.svg')}}">
                                    </a>
                                    <br>
                               
                                    <a href="tel:064 616 06 73">
                                    <img align="left" style="height: 30px;" src="/images/mobilni.png">
                                    </a>
                                    <a href="https://api.whatsapp.com/send?phone=381646160673">
                                        <img style="height: 30px;" src="{{asset('/images/icon-whatsapp.svg')}}">
                                    </a>
                                     
                        </div>
                        
                    </div>

                </div>
            </div><!-- header-content -->

            <!-- header-nav -->
            <div class="header-nav mid-header">
                <div class="container">

                    <div class="box-header-nav">

                        <!-- btn categori mobile -->
                        <span data-action="toggle-nav-cat" class="nav-toggle-menu nav-toggle-cat"><span>Categories</span></span>

                        <!-- btn menu mobile -->
                        <span data-action="toggle-nav" class="nav-toggle-menu"><span>Menu</span></span>

                        

                        <!-- menu -->
                        <div class="block-nav-menu">
                            <div class="clearfix"><span data-action="close-nav" class="close-nav"><span>close</span></span></div>
                            
                            <ul class="ui-menu">
                                <li class="parent parent-megamenu active">
                                    <a href="/">Početna</a>
                                </li>
                                @foreach($navbarKategorije as $kategorija)
                                <li class="parent parent-submenu">

                                        @if(in_array($kategorija->id, $glavneKojeSeMoguIzabrati))
                                            <a href="/prodavnica?kategorija={{$kategorija->id}}">{{$kategorija->naziv}}</a>
                                        @else
                                            <a href="javascript:void(0)"> 
                                            {{$kategorija->naziv}}
                                            </a>
                                        @endif

                                        @if(!empty( $kategorija->children))
                                        <span class="toggle-submenu"></span>
                                        <div class="submenu drop-menu">

                                            <ul >
                                                @foreach($kategorija->children as $child)
                                                    <li><a href="/prodavnica?kategorija={{$child->id}}">{{$child->naziv}}</a></li>
                                                @endforeach
                                            </ul>

                                        </div>
                                        @endif


                                </li>
                                @endforeach
                                <li class="parent parent-submenu">
                                    <a href="javascript:void(0)"> 
                                    Brendovi
                                    </a>
                                    <span class="toggle-submenu"></span>
                                    <div class="submenu drop-menu">

                                        <ul >
                                            @foreach($navbarBrendovi as $brend)
                                                <li><a href="/prodavnica?brend={{$brend->id}}">{{$brend->naziv}}</a></li>
                                            @endforeach
                                        </ul>

                                    </div>



                                </li>

                            </ul>

                        </div><!-- menu -->

                        <!-- mini cart -->
                        <div class="block-minicart dropdown ">

                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <span class="cart-icon"></span>
                            </a>

                            <div class="dropdown-menu">
                                <form>
                                    <div  class="minicart-content-wrapper" >
                                        <div class="subtitle">
                                            You have 6 item(s) in your cart
                                        </div>
                                        <div class="minicart-items-wrapper">
                                            <ol class="minicart-items">
                                                <li class="product-item">
                                                    <a class="product-item-photo" href="#" title="The Name Product">
                                                        <img class="product-image-photo" src="{{asset('images/media/index1/minicart.jpg')}}" alt="The Name Product">
                                                    </a>
                                                    <div class="product-item-details">
                                                        <strong class="product-item-name">
                                                            <a href="#">Donec Ac Tempus</a>
                                                        </strong>
                                                        <div class="product-item-price">
                                                            <span class="price">61,19 €</span>
                                                        </div>
                                                        <div class="product-item-qty">
                                                            <span class="label">Qty: </span ><span class="number">1</span>
                                                        </div>
                                                        <div class="product-item-actions">
                                                            <a class="action delete" href="#" title="Remove item">
                                                                <span>Remove</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="product-item">
                                                    <a class="product-item-photo" href="#" title="The Name Product">
                                                        <img class="product-image-photo" src="{{asset('images/media/index1/minicart2.jpg')}}" alt="The Name Product">
                                                    </a>
                                                    <div class="product-item-details">
                                                        <strong class="product-item-name">
                                                            <a href="#">Donec Ac Tempus</a>
                                                        </strong>
                                                        <div class="product-item-price">
                                                            <span class="price">61,19 €</span>
                                                        </div>
                                                        <div class="product-item-qty">
                                                            <span class="label">Qty: </span ><span class="number">1</span>
                                                        </div>
                                                        <div class="product-item-actions">
                                                            <a class="action delete" href="#" title="Remove item">
                                                                <span>Remove</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                        <div class="subtotal">
                                            <span class="label">Total</span>
                                            <span class="price">$630</span>
                                        </div>
                                        <div class="actions">
                                            <!-- <a class="btn btn-viewcart" href="">
                                                <span>Shopping bag</span>
                                            </a> -->
                                            <button class="btn btn-checkout" type="button" title="Check Out">
                                                <span>Checkout</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                        <!-- search -->
                        <div class="block-search">
                            <div class="block-title">
                                <span>Search</span>
                            </div>
                            <div class="block-content">
                                <div class="form-search">
                                    <form>
                                        <div class="box-group">
                                            <input type="text" class="form-control" placeholder="i'm Searching for...">
                                            <button class="btn btn-search" type="button"><span>search</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- search -->
                        
                    </div>
                </div>
            </div><!-- header-nav -->

        </header><!-- end HEADER -->        

        <!-- MAIN -->
        <main class="site-main">

            <!--   Popup Newsletter   -----------
            <div class="modal fade popup-newsletter" id="popup-newsletter" tabindex="-1" role="dialog" >
                <div class="modal-dialog" role="document">
                    <div class="modal-content" style="background-image: url(images/media/index1/Popup.jpg);">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="block-newletter">
                            <div class="block-title">signup for our newsletter & promotions</div>
                            <div class="block-content">
                                <p class="text-title">GET <span>50% <span>off</span></span></p>
                                <form>
                                    <label>on your next purchase</label>
                                    <div class="input-group">
                                        <input type="text" placeholder="Enter your email..." class="form-control">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-subcribe"><span>Subscribe</span></button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="checkbox btn-checkbox">
                                <label>
                                    <input type="checkbox"><span>Dont’s show this popup again!</span>
                                </label>
                            </div>
                        </div>

                        
                        
                    </div>
                </div>
            </div> 
        -->
            @yield('main')

        </main><!-- end MAIN -->

        <!-- FOOTER -->
        <footer class="site-footer footer-opt-1">

            <div class="container">
                <div class="footer-column">
                
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-xs-6 col">
                            <strong class="logo-footer">
                                <a href="/"><img src="{{asset('images/logo.png')}}" alt="logo"></a>
                            </strong>

                            <table class="address">
                                
                                <tr>
                                    <td><i class="fa fa-phone" aria-hidden="true"></i> <b>Telefon: </b></td>
                                    <td><a href="tel:064 616 0673">064 616 0673</a></td>
                                </tr>
                                <tr>
                                    <td><br></td>
                                    <td><a href="tel:064 616 0672">064 616 0672</a></td>
                                </tr>
                                <tr>
                                    <td><br></td>
                                    <td><a href="tel:011/228-00-74">011 228 00 74</a></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-envelope" aria-hidden="true"></i><b> E-mail:</b></td>
                                    <td><a href="mailto:office@oddoigracke.rs">office@oddoigracke.rs</a></td>
                                </tr>
                                <tr>
                                    <td><br></td>
                                    <td>
                                        <img style="height: 25px;" src="{{asset('/images/icon-viber.svg')}}">
                                        <img style="height: 25px;" src="{{asset('/images/icon-whatsapp.svg')}}">
                                        <img style="height: 25px;" src="{{asset('/images/fb.png')}}">
                                        <img style="height: 25px;" src="{{asset('/images/insta.png')}}"></td>
                                    </td>
                                
                                    
                                </tr>
                                 
                            </table>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-6 col">
                            <div class="links">
                            <h3 class="title">Company</h3>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Testimonials</a></li>
                                <li><a href="#">Affiliate Program</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-6 col">
                            <div class="links">
                            <h3 class="title">My Account</h3>
                            <ul>
                                <li><a href="#">My Order</a></li>
                                <li><a href="#">My Wishlist</a></li>
                                <li><a href="#">My Credit Slip</a></li>
                                <li><a href="#">My Addresses</a></li>
                                <li><a href="#">My Account</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-6 col">
                            <div class="links">
                            <h3 class="title">Support</h3>
                            <ul>
                                <li><a href="#">New User Guide</a></li>
                                <li><a href="#">Help Center</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Report Spam</a></li>
                                <li><a href="#">FAQ</a></li>
                            </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-xs-6 col">
                            <div class="block-newletter">
                                <div class="block-title">NEWSLETTER</div>
                                <div class="block-content">
                                    <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Your Email Address">
                                        <span class="input-group-btn">
                                            <button class="btn btn-subcribe" type="button"><span>ok</span></button>
                                        </span>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="block-social">
                                <div class="block-title">Let’s Socialize </div>
                                <div class="block-content">
                                    <a href="#" class="sh-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#" class="sh-pinterest"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                                    <a href="#" class="sh-vk"><i class="fa fa-vk" aria-hidden="true"></i></a>
                                    <a href="#" class="sh-twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#" class="sh-google"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--
                <div class="payment-methods">
                    <div class="block-title">
                        Accepted Payment Methods
                    </div>
                    <div class="block-content">
                        <img alt="payment" src="{{asset('images/media/index1/payment1.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment2.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment3.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment4.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment5.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment6.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment7.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment8.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment9.png')}}">
                        <img alt="payment" src="{{asset('images/media/index1/payment10.png')}}">
                    </div>
                </div>
-->
                <div class="footer-links">
                    
                        
                    <ul class="links">
                        <li><strong class="title">HOT SEARCHED KEYWORDS:</strong></li>
                        <li><a href="#">Xiaomi Mi3 </a></li>
                        <li><a href="#">Digiflip Pro XT 712 Tablet</a></li>
                        <li><a href="#">Mi 3 Phones  </a></li>
                        <li><a href="#">Iphone 6 Plus  </a></li>
                        <li><a href="#">Women's Messenger Bags</a></li>
                        <li><a href="#">Wallets   </a></li>
                        <li><a href="#">Women's Clutches   </a></li>
                        <li><a href="#">Backpacks Totes</a></li>
                    </ul>
                    
                    
                        
                    <ul class="links">
                        <li><strong class="title">tvs: </strong></li>
                        <li><a href="#">Sony TV  </a></li>
                        <li><a href="#"> Samsung TV  </a></li>
                        <li><a href="#">LG TV  </a></li>
                        <li><a href="#">Panasonic TV  </a></li>
                        <li><a href="#"> Onida TV  </a></li>
                        <li><a href="#"> Toshiba TV  </a></li>
                        <li><a href="#"> Philips TV</a></li>
                        <li><a href="#">Micromax TV</a></li>
                        <li><a href="#">LED TV </a></li>
                        <li><a href="#">  LCD TV  </a></li>
                        <li><a href="#">Plasma TV </a></li>
                        <li><a href="#">3D TV    </a></li>
                        <li><a href="#">Smart TV </a></li>
                    </ul>
                   
                   
                        
                    <ul  class="links">
                        <li><strong class="title">MOBILES: </strong></li>
                        <li><a href="#">Moto E </a></li>
                        <li><a href="#"> Samsung Mobile </a></li>
                        <li><a href="#">Micromax Mobile</a></li>
                        <li><a href="#">Nokia Mobile </a></li>
                        <li><a href="#"> HTC Mobile </a></li>
                        <li><a href="#">Sony Mobile  </a></li>
                        <li><a href="#"> Apple Mobile  </a></li>
                        <li><a href="#"> LG Mobile  </a></li>
                        <li><a href="#">Karbonn Mobile</a></li>
                    </ul>
                   
                        
                    <ul class="links">
                        <li><strong class="title">LAPTOPS:</strong></li>
                        <li><a href="#">Apple Laptop  </a></li>
                        <li><a href="#">Acer Laptop </a></li>
                        <li><a href="#">Samsung Laptop</a></li>
                        <li><a href="#">Lenovo Laptop </a></li>
                        <li><a href="#">Sony Laptop</a></li>
                        <li><a href="#">Dell Laptop</a></li>
                        <li><a href="#">Asus Laptop </a></li>
                        <li><a href="#">  Toshiba Laptop</a></li>
                        <li><a href="#">LG Laptop </a></li>
                        <li><a href="#">HP Laptop</a></li>
                        <li><a href="#">Notebook</a></li>
                    </ul>
                   
                    
                        
                    <ul class="links">
                        <li><strong class="title">WATCHES:</strong></li>
                        <li><a href="#">FCUK Watches</a></li>
                        <li><a href="#">Titan Watches  </a></li>
                        <li><a href="#">  Casio Watches </a></li>
                        <li><a href="#">  Fastrack Watches </a></li>
                        <li><a href="#">Timex Watches </a></li>
                        <li><a href="#">Fossil Watches</a></li>
                        <li><a href="#">Diesel Watches  </a></li>
                        <li><a href="#"> Luxury Watches</a></li>
                    </ul>
                   
                    
                    <ul class="links">
                        <li><strong class="title">FOOTWEAR: </strong></li>
                        <li><a href="#">Shoes  </a></li>
                        <li><a href="#">Casual Shoes </a></li>
                        <li><a href="#"> Sports Shoes </a></li>
                        <li><a href="#">Formal Shoes </a></li>
                        <li><a href="#"> Adidas Shoes  </a></li>
                        <li><a href="#">Gas Shoes</a></li>
                        <li><a href="#"> Puma Shoes</a></li>
                        <li><a href="#">Reebok Shoes </a></li>
                        <li><a href="#">Woodland Shoes  </a></li>
                        <li><a href="#">Red tape Shoes</a></li>
                        <li><a href="#">Nike Shoes</a></li>
                    </ul>
                    
                </div>

                <div class="footer-bottom">
                    <div class="links">
                        <ul>
                            <li><a href="#">    Company Info – Partnerships    </a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Online Shopping </a></li>
                            <li><a href="#">Promotions </a></li>
                            <li><a href="#">My Orders  </a></li>
                            <li><a href="#">Help  </a></li>
                            <li><a href="#">Site Map </a></li>
                            <li><a href="#">Customer Service </a></li>
                            <li><a href="#">Support  </a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Most Populars </a></li>
                            <li><a href="#">Best Sellers  </a></li>
                            <li><a href="#">New Arrivals  </a></li>
                            <li><a href="#">Special Products  </a></li>
                            <li><a href="#"> Manufacturers     </a></li>
                            <li><a href="#">Our Stores   </a></li>
                            <li><a href="#">Shipping      </a></li>
                            <li><a href="#">Payments      </a></li>
                            <li><a href="#">Payments      </a></li>
                            <li><a href="#">Refunds  </a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Terms & Conditions  </a></li>
                            <li><a href="#">Policy      </a></li>
                            <li><a href="#">Policy      </a></li>
                            <li><a href="#"> Shipping     </a></li>
                            <li><a href="#">Payments      </a></li>
                            <li><a href="#">Returns      </a></li>
                            <li><a href="#">Refunds      </a></li>
                            <li><a href="#">Warrantee      </a></li>
                            <li><a href="#">FAQ      </a></li>
                            <li><a href="#">Contact  </a></li>
                        </ul>
                    </div>
                </div>

                <div class="copyright">
                    
                    Copyright © 2019 Puzzle Shop</a>
                   
                </div>

            </div>

        </footer><!-- end FOOTER -->        
        
        <!--back-to-top  -->
        <a href="#" class="back-to-top">
            <i aria-hidden="true" class="fa fa-angle-up"></i>
        </a>
        
    </div>

    <!-- jQuery -->    
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

    <!-- sticky -->
    <script type="text/javascript" src="{{asset('js/jquery.sticky.js')}}"></script>

    <!-- Boostrap --> 
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <!--jquery Bxslider  -->
    <script type="text/javascript" src="{{asset('js/jquery.bxslider.min.js')}}"></script>
    
    <!-- actual --> 
    <script type="text/javascript" src="{{asset('js/jquery.actual.min.js')}}"></script>

    <!-- jQuery UI -->
    <script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
    
    
    <!-- parallax jquery--> 
    <script type="text/javascript" src="{{asset('js/jquery.parallax-1.1.3.js')}}"></script>

    <!-- arcticmodal -->
    <script src="{{asset('js/arcticmodal/jquery.arcticmodal.js')}}"></script>
    
    <!-- Main -->  
    <script type="text/javascript" src="{{asset('js/arcticmodal/jquery.arcticmodal.js')}}"></script>


    <!-- OWL CAROUSEL Slider -->    
    <script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>

    <!-- Countdown --> 
    <script type="text/javascript" src="{{asset('js/jquery.countdown.min.js')}}"></script>
    
    <!-- Chosen jquery-->    
    <script type="text/javascript" src="{{asset('js/chosen.jquery.min.js')}}"></script>


    <!-- elevatezoom --> 
    <script type="text/javascript" src="{{asset('js/jquery.elevateZoom.min.js')}}"></script>

    <!-- fancybox -->
    <script src="{{asset('js/fancybox/source/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('js/fancybox/source/helpers/jquery.fancybox-media.js')}}"></script>
    <script src="{{asset('js/fancybox/source/helpers/jquery.fancybox-thumbs.js')}}"></script>
    

    <!-- arcticmodal -->
    <script src="{{asset('js/arcticmodal/jquery.arcticmodal.js')}}"></script>
    
    <!-- Main -->  
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>

    <!-- Custom scripts -->

    <script>

        (function($) {

            "use strict";

            $(document).ready(function() {

                /*  [ Filter by price ]

                - - - - - - - - - - - - - - - - - - - - */

                $('#slider-range').slider({

                    range: true,

                    min: 0,

                    max: 500,

                    values: [0, 300],

                    slide: function (event, ui) {

                        $('#amount-left').text(ui.values[0] );
                        $('#amount-right').text(ui.values[1] );

                    }

                });

                $('#amount-left').text($('#slider-range').slider('values', 0));

                $('#amount-right').text($('#slider-range').slider('values', 1));
            }); 

        })(jQuery);

    </script>

    @yield('scriptsBottom')


</body>

</html>